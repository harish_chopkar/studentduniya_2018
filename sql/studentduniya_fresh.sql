-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 18, 2017 at 09:06 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `studentduniya_fresh`
--

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE IF NOT EXISTS `contact_us` (
  `id` int(11) NOT NULL,
  `fname` varchar(30) NOT NULL,
  `lname` varchar(30) NOT NULL,
  `education` varchar(50) NOT NULL,
  `college_name` varchar(200) NOT NULL,
  `email` varchar(150) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `message` varchar(300) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `fname`, `lname`, `education`, `college_name`, `email`, `mobile`, `message`, `created`) VALUES
(1, 'harish', 'chopkar', '12', 'mvm', 'harry.harish333@gmail.com', '7387990061', 'welcome ', '0000-00-00 00:00:00'),
(2, 'adsa', 'asd', '12', 'mvm', 'harry.harish45@gmail.com', '7323', 'asd', '0000-00-00 00:00:00'),
(4, 'asdsad', 'hary', 'qw', 'w', '33@gmail.com', '33', 'as', '0000-00-00 00:00:00'),
(5, 'komal', 'ko', '12th', 'mvm', 'kate@gmail.com', '1234567898', 'vxcvxcvxcv', '2017-09-14 10:53:08'),
(6, 'ankita', 'bhatt', 'BE', 'MVM', 'ankita@gmail.com', '1234367898', 'abcgh', '2017-09-16 10:02:39');

-- --------------------------------------------------------

--
-- Table structure for table `nagpur_university_courses`
--

CREATE TABLE IF NOT EXISTS `nagpur_university_courses` (
  `id` int(11) NOT NULL,
  `course_name` varchar(150) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=967 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nagpur_university_courses`
--

INSERT INTO `nagpur_university_courses` (`id`, `course_name`) VALUES
(1, 'B. Arch. (I.D.) Eighth Semester '),
(516, 'B. Arch. (I.D.) Ninth Semester '),
(517, 'B. Arch. (I.D.) Tenth Semester '),
(518, 'B. Arch. Eighth Semester'),
(519, 'B. Arch. Eighth Semester CBS'),
(520, 'B. Arch. Fifth Semester [CBS] '),
(521, 'B. Arch. First Semester [CBS]'),
(522, 'B. Arch. Fourth Semester [CBS] '),
(523, 'B. Arch. Nineth Semester [CBS]'),
(524, 'B. Arch. Ninth Semester '),
(525, 'B. Arch. Second Semester [CBS]'),
(526, 'B. Arch. Seventh Semester [CBS]'),
(527, 'B. Arch. Sixth Semester'),
(528, 'B. Arch. Tenth Semester '),
(529, 'B. Arch. Tenth Semester [CBS]'),
(530, 'B. Arch. Third Semester [CBS]'),
(531, 'B.E. Eighth Semester (Aeronautical Engineering) [CBS]'),
(532, 'B.E. Eighth Semester (Civil Engineering)'),
(533, 'B.E. Eighth Semester (Civil Engineering) [CBS]'),
(534, 'B.E. Eighth Semester (Computer Engineering)'),
(535, 'B.E. Eighth Semester (Computer Engineering) [CBS]'),
(536, 'B.E. Eighth Semester (Computer Science &amp; Engineering)'),
(537, 'B.E. Eighth Semester (Computer Science &amp; Engineering) [CBS]'),
(538, 'B.E. Eighth Semester (Computer Technology)'),
(539, 'B.E. Eighth Semester (Computer Technology) [CBS]'),
(540, 'B.E. Eighth Semester (Electrical Engineering) (E&amp;P)'),
(541, 'B.E. Eighth Semester (Electrical Engineering) (E&amp;P) [CBS]'),
(542, 'B.E. Eighth Semester (Electronics &amp; Communication)'),
(543, 'B.E. Eighth Semester (Electronics &amp; Communication) [CBS]'),
(544, 'B.E. Eighth Semester (Electronics &amp; Telecommunication)'),
(545, 'B.E. Eighth Semester (Electronics &amp; Telecommunication) [CBS]'),
(546, 'B.E. Eighth Semester (Electronics Engineering)'),
(547, 'B.E. Eighth Semester (Electronics Engineering) [CBS]'),
(548, 'B.E. Eighth Semester (Information Technology)'),
(549, 'B.E. Eighth Semester (Information Technology) [CBS]'),
(550, 'B.E. Eighth Semester (Mechanical Engineering)'),
(551, 'B.E. Eighth Semester (Mechanical Engineering) [CBS]'),
(552, 'B.E. Eighth Semester (Power Engineering) [CBS]'),
(553, 'B.E. Fifth Semester (Aeronautical Engineering) [CBS]'),
(554, 'B.E. Fifth Semester (Civil Engineering) [CBS]'),
(555, 'B.E. Fifth Semester (Computer Engineering) [CBS]'),
(556, 'B.E. Fifth Semester (Computer Science Engineering) [CBS]'),
(557, 'B.E. Fifth Semester (Computer Technology) [CBS]'),
(558, 'B.E. Fifth Semester (Electrical Engineering) (E&amp;P) [CBS]'),
(559, 'B.E. Fifth Semester (Electronics &amp; Communication)[CBS]'),
(560, 'B.E. Fifth Semester (Electronics &amp; Telecommunication)[CBS]'),
(561, 'B.E. Fifth Semester (Electronics Engineering) [CBS]'),
(562, 'B.E. Fifth Semester (Information Technology) [CBS]'),
(563, 'B.E. Fifth Semester (Instrumentation)'),
(564, 'B.E. Fifth Semester (Mechanical Engineering)[CBS]'),
(565, 'B.E. Fifth Semester (Power Engineering) [CBS]'),
(566, 'B.E. First Semester  [CBS]'),
(567, 'B.E. First Semester (Fire Engineering) [CBS]'),
(568, 'B.E. Fourth Semester (Aeronautical Engineering) [CBS]'),
(569, 'B.E. Fourth Semester (Civil Engineering) [CBS]'),
(570, 'B.E. Fourth Semester (Computer Engineering) [CBS]'),
(571, 'B.E. Fourth Semester (Computer Science &amp; Engineering) [CBS]'),
(572, 'B.E. Fourth Semester (Computer Technology) [CBS]'),
(573, 'B.E. Fourth Semester (Electrical Engineering) (E&amp;P) [CBS]'),
(574, 'B.E. Fourth Semester (Electronics &amp; Communication) [CBS]'),
(575, 'B.E. Fourth Semester (Electronics &amp; Telecommunication) [CBS]'),
(576, 'B.E. Fourth Semester (Electronics Engineering) [CBS]'),
(577, 'B.E. Fourth Semester (Fire Engineering)  [CBS]'),
(578, 'B.E. Fourth Semester (Information Technology) [CBS]'),
(579, 'B.E. Fourth Semester (Mechanical Engineering) [CBS]'),
(580, 'B.E. Fourth Semester (Power Engineering) [CBS]'),
(581, 'B.E. Second Semester  [CBS]'),
(582, 'B.E. Second Semester (Fire Engineering)  [CBS]'),
(583, 'B.E. Seventh Semester (Aeronautical Engineering) [CBS]'),
(584, 'B.E. Seventh Semester (Civil Engineering)'),
(585, 'B.E. Seventh Semester (Civil Engineering) [CBS]'),
(586, 'B.E. Seventh Semester (Computer Engineering)'),
(587, 'B.E. Seventh Semester (Computer Science &amp; Engineering)'),
(588, 'B.E. Seventh Semester (Computer Science &amp; Engineering) [CBS]'),
(589, 'B.E. Seventh Semester (Computer Technology)'),
(590, 'B.E. Seventh Semester (Computer Technology) [CBS]'),
(591, 'B.E. Seventh Semester (Electrical Engineering) (E&amp;P)'),
(592, 'B.E. Seventh Semester (Electrical Engineering) (E&amp;P) [CBS]'),
(593, 'B.E. Seventh Semester (Electronics &amp; Communication)'),
(594, 'B.E. Seventh Semester (Electronics &amp; Communication) [CBS]'),
(595, 'B.E. Seventh Semester (Electronics &amp; Telecommunication)'),
(596, 'B.E. Seventh Semester (Electronics &amp; Telecommunication) [CBS]'),
(597, 'B.E. Seventh Semester (Electronics Engineering)'),
(598, 'B.E. Seventh Semester (Electronics Engineering) [CBS]'),
(599, 'B.E. Seventh Semester (Information Technology)'),
(600, 'B.E. Seventh Semester (Information Technology) [CBS]'),
(601, 'B.E. Seventh Semester (Instrumentation)'),
(602, 'B.E. Seventh Semester (Mechanical Engineering)'),
(603, 'B.E. Seventh Semester (Mechanical Engineering) [CBS]'),
(604, 'B.E. Seventh Semester (Power Engineering)'),
(605, 'B.E. Seventh Semester (Power Engineering) [CBS]'),
(606, 'B.E. Sixth Semester (Aeronautical Engineering) [CBS]'),
(607, 'B.E. Sixth Semester (Civil Engineering) [CBS]'),
(608, 'B.E. Sixth Semester (Computer Engineering) [CBS]'),
(609, 'B.E. Sixth Semester (Computer Science &amp; Engineering)[CBS]'),
(610, 'B.E. Sixth Semester (Computer Technology) [CBS]'),
(611, 'B.E. Sixth Semester (Electrical Engineering)(E &amp; P) [CBS]'),
(612, 'B.E. Sixth Semester (Electronic &amp; Communication) [CBS]'),
(613, 'B.E. Sixth Semester (Electronic &amp; Telecommunication) [CBS]'),
(614, 'B.E. Sixth Semester (Electronic Engineering) [CBS]'),
(615, 'B.E. Sixth Semester (Information Technology) [CBS]'),
(616, 'B.E. Sixth Semester (Instrumentation)'),
(617, 'B.E. Sixth Semester (Mechanical Engineering)[CBS]'),
(618, 'B.E. Sixth Semester (Power Engineering) [CBS]'),
(619, 'B.E. Third Semester (Aeronautical Engineering) [CBS]'),
(620, 'B.E. Third Semester (Civil Engineering) [CBS]'),
(621, 'B.E. Third Semester (Computer Engineering) [CBS]'),
(622, 'B.E. Third Semester (Computer Science &amp; Engineering)[CBS]'),
(623, 'B.E. Third Semester (Computer Technology) [CBS]'),
(624, 'B.E. Third Semester (Electrical  Engineering) (E&amp;P) [CBS]'),
(625, 'B.E. Third Semester (Electronics &amp; Communication) [CBS]'),
(626, 'B.E. Third Semester (Electronics &amp; Telecommunication)[CBS]'),
(627, 'B.E. Third Semester (Electronics Engineering) [CBS]'),
(628, 'B.E. Third Semester (Fire Engineering)  [CBS]'),
(629, 'B.E. Third Semester (Information Technology) [CBS]'),
(630, 'B.E. Third Semester (Mechanical Engineering) [CBS]'),
(631, 'B.E. Third Semester (Power Engineering) [CBS]'),
(632, 'B.Sc. (Information Technology) Fifth Semester'),
(633, 'B.Sc. (Information Technology) First Semester'),
(634, 'B.Sc. (Information Technology) Fourth Semester'),
(635, 'B.Sc. (Information Technology) Part-I'),
(636, 'B.Sc. (Information Technology) Part-III'),
(637, 'B.Sc. (Information Technology) Second Semester'),
(638, 'B.Sc. (Information Technology) Sixth Semester'),
(639, 'B.Sc. (Information Technology) Third Semester'),
(640, 'B.Sc. Fifth Semester '),
(641, 'B.Sc. Final'),
(642, 'B.Sc. First Semester'),
(643, 'B.Sc. Fourth Semester'),
(644, 'B.Sc. Fourth Semester(Absorp)'),
(645, 'B.Sc. Second Semester'),
(646, 'B.Sc. Sixth Semester'),
(647, 'B.Sc. Third Semester '),
(648, 'B.Sc. Third Semester (Absorp)'),
(649, 'B.Tech. (Biotechnology) Eigth Semester [CBS]'),
(650, 'B.Tech. (Biotechnology) Fifth Semester [CBS]'),
(651, 'B.Tech. (Biotechnology) First Semester [CBS]'),
(652, 'B.Tech. (Biotechnology) Fourth Semester [CBS]'),
(653, 'B.Tech. (Biotechnology) Second Semester [CBS]'),
(654, 'B.Tech. (Biotechnology) Seventh Semester [CBS]'),
(655, 'B.Tech. (Biotechnology) Sixth Semester [CBS]'),
(656, 'B.Tech. (Biotechnology) Third Semester [CBS]'),
(657, 'B.Tech. (Chemical Engineering) Eighth Semester'),
(658, 'B.Tech. (Chemical Engineering) Eighth Semester [CBS]'),
(659, 'B.Tech. (Chemical Engineering) Fifth Semester [CBS]'),
(660, 'B.Tech. (Chemical Engineering) First Semester [CBS]'),
(661, 'B.Tech. (Chemical Engineering) Fourth Semester [CBS]'),
(662, 'B.Tech. (Chemical Engineering) Second Semester [CBS]'),
(663, 'B.Tech. (Chemical Engineering) Seventh Semester'),
(664, 'B.Tech. (Chemical Engineering) Seventh Semester [CBS]'),
(665, 'B.Tech. (Chemical Engineering) Sixth Semester [CBS]'),
(666, 'B.Tech. (Chemical Engineering) Third Semester [CBS]'),
(667, 'B.Tech. (Chemical Technology) Eighth Semester (Food Technology) (CBS)'),
(668, 'B.Tech. (Chemical Technology) Eighth Semester (Petrol Reffing &amp; Petrochem) (CBS)'),
(669, 'B.Tech. (Chemical Technology) Eighth Semester (Plastics and Polymer Technology)'),
(670, 'B.Tech. (Chemical Technology) Eighth Semester (Plastics and Polymer Technology) (CBS)'),
(671, 'B.Tech. (Chemical Technology) Eighth Semester (Pulp &amp; Paper Technology) (CBS)'),
(672, 'B.Tech. (Chemical Technology) Eighth Semester (Surface Coating Technology) (CBS)'),
(673, 'B.Tech. (Chemical Technology) Eighth Semester (Technology of Oil, Fats and Surfactants)'),
(674, 'B.Tech. (Chemical Technology) Eighth Semester (Technology of Oil, Fats and Surfactants) (CBS)'),
(675, 'B.Tech. (Chemical Technology) Fifth Semester (Food Technology) [CBS]'),
(676, 'B.Tech. (Chemical Technology) Fifth Semester (Petro Refining &amp; Petrochem.) [CBS]'),
(677, 'B.Tech. (Chemical Technology) Fifth Semester (Plastics &amp; Polymer Technology) [CBS]'),
(678, 'B.Tech. (Chemical Technology) Fifth Semester (Pulp &amp; Paper Technology ) [CBS]'),
(679, 'B.Tech. (Chemical Technology) Fifth Semester (Surface Coating Technology) [CBS]'),
(680, 'B.Tech. (Chemical Technology) Fifth Semester (Technology of Oil,Fats &amp; Surfactants) [CBS]'),
(681, 'B.Tech. (Chemical Technology) First Semester (Food Technology) [CBS]'),
(682, 'B.Tech. (Chemical Technology) First Semester (Petro Refining &amp; Petrochem.) [CBS]'),
(683, 'B.Tech. (Chemical Technology) First Semester (Plastics and Polymer Technology) [CBS]'),
(684, 'B.Tech. (Chemical Technology) First Semester (Pulp &amp; Paper Technology) [CBS]'),
(685, 'B.Tech. (Chemical Technology) First Semester (Surface Coating Technology) [CBS]'),
(686, 'B.Tech. (Chemical Technology) First Semester (Technology of Oil, Fats and Surfactants) [CBS]'),
(687, 'B.Tech. (Chemical Technology) Fourth Semester (Food Technology) [CBS]'),
(688, 'B.Tech. (Chemical Technology) Fourth Semester (Petrol Reffing &amp; Petrochem) [CBS]'),
(689, 'B.Tech. (Chemical Technology) Fourth Semester (Plastics and Polymer Technology) [CBS]'),
(690, 'B.Tech. (Chemical Technology) Fourth Semester (Pulp &amp; Paper Technology) [CBS]'),
(691, 'B.Tech. (Chemical Technology) Fourth Semester (Surface Coating Technology) [CBS]'),
(692, 'B.Tech. (Chemical Technology) Fourth Semester (Technology of Oil, Fats and Surfactants) [CBS]'),
(693, 'B.Tech. (Chemical Technology) Second Semester (Food technology) [CBS]'),
(694, 'B.Tech. (Chemical Technology) Second Semester (Petro Refining &amp; Petrochem.) [CBS]'),
(695, 'B.Tech. (Chemical Technology) Second Semester (Plastics &amp; Polymer Technology) [CBS]'),
(696, 'B.Tech. (Chemical Technology) Second Semester (Pulp &amp; Paper Technology) [CBS]'),
(697, 'B.Tech. (Chemical Technology) Second Semester (Surface Coating technology) [CBS]'),
(698, 'B.Tech. (Chemical Technology) Second Semester (Technology of Oil,Fats &amp; Surfactants) [CBS]'),
(699, 'B.Tech. (Chemical Technology) Seventh Semester (Food Technology) [CBS]'),
(700, 'B.Tech. (Chemical Technology) Seventh Semester (Petro Refining &amp; Petrochem.) [CBS]'),
(701, 'B.Tech. (Chemical Technology) Seventh Semester (Plastics &amp; Polymer Technology)'),
(702, 'B.Tech. (Chemical Technology) Seventh Semester (Plastics &amp; Polymer Technology) [CBS]'),
(703, 'B.Tech. (Chemical Technology) Seventh Semester (Pulp &amp; Paper Technology)[CBS]'),
(704, 'B.Tech. (Chemical Technology) Seventh Semester (Surface Coating technology) [CBS]'),
(705, 'B.Tech. (Chemical Technology) Seventh Semester (Technology of Oil,Fats &amp; Surfactants)'),
(706, 'B.Tech. (Chemical Technology) Seventh Semester (Technology of Oil,Fats &amp; Surfactants) [CBS]'),
(707, 'B.Tech. (Chemical Technology) Sixth Semester (Food Technology) [CBS]'),
(708, 'B.Tech. (Chemical Technology) Sixth Semester (Petro Refining &amp; Petrochem.) [CBS]'),
(709, 'B.Tech. (Chemical Technology) Sixth Semester (Plastics &amp; Polymer Technology) [CBS]'),
(710, 'B.Tech. (Chemical Technology) Sixth Semester (Pulp &amp; Paper Technology ) [CBS]'),
(711, 'B.Tech. (Chemical Technology) Sixth Semester (Surface Coating Technology) [CBS]'),
(712, 'B.Tech. (Chemical Technology) Sixth Semester (Technology of Oil,Fats &amp; Surfactants) [CBS]'),
(713, 'B.Tech. (Chemical Technology) Third Semester (Food Technology) [CBS]'),
(714, 'B.Tech. (Chemical Technology) Third Semester (Petrol Reffing &amp; Petrochem) [CBS]'),
(715, 'B.Tech. (Chemical Technology) Third Semester (Plastics and Polymer Technology) [CBS]'),
(716, 'B.Tech. (Chemical Technology) Third Semester (Pulp &amp; Paper Technology) [CBS]'),
(717, 'B.Tech. (Chemical Technology) Third Semester (Surface Coating Technology) [CBS]'),
(718, 'B.Tech. (Chemical Technology) Third Semester (Technology of Oil, Fats and Surfactants) [CBS]'),
(719, 'Bachelor of Computer Application Fifth Semester'),
(720, 'Bachelor of Computer Application Final'),
(721, 'Bachelor of Computer Application First Semester'),
(722, 'Bachelor of Computer Application Fourth Semester'),
(723, 'Bachelor of Computer Application Part-II'),
(724, 'Bachelor of Computer Application Second Semester'),
(725, 'Bachelor of Computer Application Sixth Semester (Revised)'),
(726, 'Bachelor of Computer Application Third Semester'),
(727, 'Bachelor of Pharmacy Eighth Semester [CBS]'),
(728, 'Bachelor of Pharmacy Fifth Semester [CBS]'),
(729, 'Bachelor of Pharmacy Final Year '),
(730, 'Bachelor of Pharmacy First Semester [CBS]'),
(731, 'Bachelor of Pharmacy First Year'),
(732, 'Bachelor of Pharmacy Fourth Semester [CBS]'),
(733, 'Bachelor of Pharmacy Second Semester [CBS]'),
(734, 'Bachelor of Pharmacy Second Year'),
(735, 'Bachelor of Pharmacy Seventh Semester [CBS]'),
(736, 'Bachelor of Pharmacy Sixth Semester [CBS]'),
(737, 'Bachelor of Pharmacy Third Semester [CBS]'),
(738, 'Bachelor of Pharmacy Third Year '),
(739, 'Bachelor of Science in Forensic Science Part -I (First Semester)'),
(740, 'Bachelor of Science in Forensic Science Part -I (Second Semester)'),
(741, 'Bachelor of Science in Forensic Science Part -II (Fourth Semester)'),
(742, 'Bachelor of Science in Forensic Science Part -II (Third Semester)'),
(743, 'Bachelor of Science in Forensic Science Part -III (Fifth Semester )'),
(744, 'Bachelor of Science in Forensic Science Part -III (Sixth Semester)'),
(745, 'Diploma in Sericulture &amp; Pest Management'),
(746, 'M. Arch. (Environmental Architecture) First Semester [CBS]'),
(747, 'M. Arch. (Environmental Architecture) Second Semester [CBCS]'),
(748, 'M. Arch. (Environmental Architecture) Third Semester [CBS]'),
(749, 'M. Sc. (Bio-Chemistry) First Semester [CBCS]'),
(750, 'M. Sc. (Bio-Chemistry) First Semester [CBS]'),
(751, 'M. Sc. (Bio-Chemistry) Fourth Semester [CBCS]'),
(752, 'M. Sc. (Bio-Chemistry) Fourth Semester [CBS]'),
(753, 'M. Sc. (Bio-Chemistry) Second Semester [CBCS]'),
(754, 'M. Sc. (Bio-Chemistry) Second Semester [CBS]'),
(755, 'M. Sc. (Bio-Chemistry) Third Semester [CBCS]'),
(756, 'M. Sc. (Bio-Chemistry) Third Semester [CBS]'),
(757, 'M. Sc. (Botany) First Semester [CBCS]'),
(758, 'M. Sc. (Botany) First Semester [CBS]'),
(759, 'M. Sc. (Botany) Fourth Semester [CBS]'),
(760, 'M. Sc. (Botany) Fourth Semester [CBSC]'),
(761, 'M. Sc. (Botany) Part-II'),
(762, 'M. Sc. (Botany) Second Semester [CBCS]'),
(763, 'M. Sc. (Botany) Second Semester [CBS]'),
(764, 'M. Sc. (Botany) Third Semester [CBCS]'),
(765, 'M. Sc. (Botany) Third Semester [CBS]'),
(766, 'M. Sc. (Chemistry) First Semester [CBCS]'),
(767, 'M. Sc. (Chemistry) First Semester [CBS]'),
(768, 'M. Sc. (Chemistry) Fourth Semester [CBCS]'),
(769, 'M. Sc. (Chemistry) Fourth Semester [CBS]'),
(770, 'M. Sc. (Chemistry) Part-II'),
(771, 'M. Sc. (Chemistry) Second Semester [CBCS]'),
(772, 'M. Sc. (Chemistry) Second Semester [CBS]'),
(773, 'M. Sc. (Chemistry) Third Semester [CBCS]'),
(774, 'M. Sc. (Chemistry) Third Semester [CBS]'),
(775, 'M. Sc. (Computer Science) First Semester [CBCS]'),
(776, 'M. Sc. (Computer Science) Fourth Semester [CBCS]'),
(777, 'M. Sc. (Computer Science) Fourth Semester [CBS]'),
(778, 'M. Sc. (Computer Science) Second Semester [CBCS]'),
(779, 'M. Sc. (Computer Science) Second Semester [CBS]'),
(780, 'M. Sc. (Computer Science) Third Semester [CBCS]'),
(781, 'M. Sc. (Computer Science) Third Semester [CBS]'),
(782, 'M. Sc. (Computer Science)(New) Final'),
(783, 'M. Sc. (Electronics) First Semester [CBS]'),
(784, 'M. Sc. (Electronics) Fourth Semester [CBCS]'),
(785, 'M. Sc. (Electronics) Second Semester [CBCS]'),
(786, 'M. Sc. (Electronics) Third Semester [CBCS]'),
(787, 'M. Sc. (Electronics) Third Semester [CBS]'),
(788, 'M. Sc. (Env. Science) First Semester [CBCS]'),
(789, 'M. Sc. (Env. Science) Fourth Semester [CBCS]'),
(790, 'M. Sc. (Env. Science) Second Semester [CBCS]'),
(791, 'M. Sc. (Env. Science) Third Semester [CBCS]'),
(792, 'M. Sc. (Forensic Science)  Fourth Semester [CBS]'),
(793, 'M. Sc. (Forensic Science)  Second Semester [CBS]'),
(794, 'M. Sc. (Forensic Science)  Third Semester [CBS]'),
(795, 'M. Sc. (Geology) First Semester [CBCS]'),
(796, 'M. Sc. (Geology) Fourth Semester [CBCS]'),
(797, 'M. Sc. (Geology) Fourth Semester [CBS]'),
(798, 'M. Sc. (Geology) Part II '),
(799, 'M. Sc. (Geology) Second Semester [CBCS]'),
(800, 'M. Sc. (Geology) Second Semester [CBS]'),
(801, 'M. Sc. (Geology) Third Semester [CBS]'),
(802, 'M. Sc. (Information Technology) Fourth Semester [CBCS]'),
(803, 'M. Sc. (Mathematics) First Semester [CBCS]'),
(804, 'M. Sc. (Mathematics) First Semester [CBS]'),
(805, 'M. Sc. (Mathematics) Fourth Semester [CBCS]'),
(806, 'M. Sc. (Mathematics) Fourth Semester [CBS]'),
(807, 'M. Sc. (Mathematics) Second Semester [CBCS]'),
(808, 'M. Sc. (Mathematics) Second Semester [CBS]'),
(809, 'M. Sc. (Mathematics) Third Semester [CBCS]'),
(810, 'M. Sc. (Microbiology) First Semester [CBS]'),
(811, 'M. Sc. (Microbiology) Fourth Semester [CBCS]'),
(812, 'M. Sc. (Microbiology) Fourth Semester [CBS]'),
(813, 'M. Sc. (Microbiology) Part II'),
(814, 'M. Sc. (Microbiology) Second Semester [CBCS]'),
(815, 'M. Sc. (Microbiology) Second Semester [CBS]'),
(816, 'M. Sc. (Microbiology) Third Semester [CBCS]'),
(817, 'M. Sc. (Physics) (New) Part-II'),
(818, 'M. Sc. (Physics) First Semester [CBCS]'),
(819, 'M. Sc. (Physics) First Semester [CBS]'),
(820, 'M. Sc. (Physics) Fourth Semester [CBCS]'),
(821, 'M. Sc. (Physics) Fourth Semester [CBS]'),
(822, 'M. Sc. (Physics) Second Semester [CBCS]'),
(823, 'M. Sc. (Physics) Second Semester [CBS]'),
(824, 'M. Sc. (Physics) Third Semester [CBCS]'),
(825, 'M. Sc. (Physics) Third Semester [CBS]'),
(826, 'M. Sc. (Statistics) First Semester [CBCS]'),
(827, 'M. Sc. (Statistics) First Semester [CBS]'),
(828, 'M. Sc. (Statistics) Fourth Semester [CBCS]'),
(829, 'M. Sc. (Statistics) Second Semester [CBCS]'),
(830, 'M. Sc. (Statistics) Second Semester [CBS]'),
(831, 'M. Sc. (Statistics) Third Semester [CBCS]'),
(832, 'M. Sc. (Statistics) Third Semester [CBS]'),
(833, 'M. Sc. (Tech.) (App. Geology) Fifth Semester [CBS]'),
(834, 'M. Sc. (Tech.) (App. Geology) First Semester [CBCS]'),
(835, 'M. Sc. (Tech.) (App. Geology) Fourth Semester [CBCS]'),
(836, 'M. Sc. (Tech.) (App. Geology) Second Semester [CBCS]'),
(837, 'M. Sc. (Tech.) (App. Geology) Second Semester [CBS]'),
(838, 'M. Sc. (Tech.) (App. Geology) Sixth Semester [CBS]'),
(839, 'M. Sc. (Tech.) (App. Geology) Third Semester [CBCS]'),
(840, 'M. Sc. (Tech.) (App. Geology) Third Semester [CBS]'),
(841, 'M. Sc. (Zoology) First Semester [CBCS]'),
(842, 'M. Sc. (Zoology) First Semester [CBS]'),
(843, 'M. Sc. (Zoology) Fourth Semester [CBCS]'),
(844, 'M. Sc. (Zoology) Fourth Semester [CBS]'),
(845, 'M. Sc. (Zoology) Part-II'),
(846, 'M. Sc. (Zoology) Second Semester [CBCS]'),
(847, 'M. Sc. (Zoology) Second Semester [CBS]'),
(848, 'M. Sc. (Zoology) Third Semester [CBCS]'),
(849, 'M. Sc. (Zoology) Third Semester [CBS]'),
(850, 'M. Sc. Molecular Biology and Genetic Engineering Semester I (CBCS)  '),
(851, 'M. Sc. Molecular Biology and Genetic Engineering Semester II (CBCS)'),
(852, 'M. Sc. Molecular Biology and Genetic Engineering Semester III (CBCS)'),
(853, 'M. Sc. Molecular Biology and Genetic Engineering Semester IV (CBCS)'),
(854, 'M. Sc. Tech Final (App. Geology)'),
(855, 'M. Sc.(Bio-Technology) First Semester [CBCS]'),
(856, 'M. Sc.(Bio-Technology) First Semester [CBS]'),
(857, 'M. Sc.(Bio-Technology) Fourth Semester [CBCS]'),
(858, 'M. Sc.(Bio-Technology) Second Semester [CBCS]'),
(859, 'M. Sc.(Bio-Technology) Second Semester [CBS]'),
(860, 'M. Sc.(Bio-Technology) Third Semester [CBCS]'),
(861, 'M. Sc.(Bio-Technology) Third Semester [CBS]'),
(862, 'M.E. First Semester (Wireless Communication &amp; Computing)'),
(863, 'M.E. Fourth Semester (Winter 2016)'),
(864, 'M.E. Second Semester (Embedded System &amp; Computing)'),
(865, 'M.E. Second Semester (Wireless Communication &amp; Computing)'),
(866, 'M.E. Second Semester (Wireless Communication &amp; Computing) [CBCS]'),
(867, 'M.E. Third Semester  (Wireless Communication &amp; Computing)'),
(868, 'M.E. Third Semester (Embedded System &amp; Computing)'),
(869, 'M.Sc. (Medicinal Plant) First Semester [CBCS]'),
(870, 'M.Sc. (Medicinal Plant) First Semester [CBS]'),
(871, 'M.Sc. (Medicinal Plant) Fourth Semester [CBS]'),
(872, 'M.Sc. (Medicinal Plant) Second Semester [CBCS]'),
(873, 'M.Sc. (Medicinal Plant) Third Semester [CBS]'),
(874, 'M.Tech. First Semester (C.A.D / C.A.M)'),
(875, 'M.Tech. First Semester (C.A.D / C.A.M) [CBCS]'),
(876, 'M.Tech. First Semester (Chemical Engineering) [CBCS]'),
(877, 'M.Tech. First Semester (Chemical Technology)(Food Technology) [CBCS]'),
(878, 'M.Tech. First Semester (Chemical Technology)(Paints Technology) [CBCS]'),
(879, 'M.Tech. First Semester (Chemical Technology)(Petro Chemical Technology)'),
(880, 'M.Tech. First Semester (Chemical Technology)(Petro Chemical Technology) [CBCS]'),
(881, 'M.Tech. First Semester (Computer Science &amp; Engineering)'),
(882, 'M.Tech. First Semester (Computer Science &amp; Engineering) [CBCS]'),
(883, 'M.Tech. First Semester (Electronics Engineering)'),
(884, 'M.Tech. First Semester (Electronics Engineering) [CBCS]'),
(885, 'M.Tech. First Semester (Heat Power Engineering)'),
(886, 'M.Tech. First Semester (Heat Power Engineering) [CBCS]'),
(887, 'M.Tech. First Semester (Integrated Power System)'),
(888, 'M.Tech. First Semester (Integrated Power System) [CBCS]'),
(889, 'M.Tech. First Semester (Mechanical Engineering Design) [CBCS]'),
(890, 'M.Tech. First Semester (Power Electronics And Power System)'),
(891, 'M.Tech. First Semester (Power Electronics And Power System) [CBCS]'),
(892, 'M.Tech. First Semester (Structural Engineering)'),
(893, 'M.Tech. First Semester (Structural Engineering) [CBCS]'),
(894, 'M.Tech. First Semester (V.L.S.I.)'),
(895, 'M.Tech. First Semester (V.L.S.I.) [CBCS]'),
(896, 'M.Tech. First Semester Electrical Engineering (Industrial Drives &amp; Control) [CBCS]'),
(897, 'M.Tech. First Semester Electronics Engineering (Communication)'),
(898, 'M.Tech. First Semester Electronics Engineering (Communication) [CBCS]'),
(899, 'M.Tech. First Semester of Software System [CBS]'),
(900, 'M.Tech. Fourth Semester (CAD/CAM) (Winter 2016)'),
(901, 'M.Tech. Fourth Semester (Civil Engineering)(Environmenal)(Winter 2016)'),
(902, 'M.Tech. Fourth Semester (Electronics Engineering)(Communication)(Winter 2016)'),
(903, 'M.Tech. Fourth Semester (Energy Management System)(Winter 2016)'),
(904, 'M.Tech. Fourth Semester (Integrated Power System)(Winter 2016)'),
(905, 'M.Tech. Fourth Semester (Mechanical Engineering Design)(Winter 2016)'),
(906, 'M.Tech. Fourth Semester (Power Electronics And Power System)(Winter 2016)'),
(907, 'M.Tech. Fourth Semester (V.L.S.I.)(Winter 2016)'),
(908, 'M.Tech. Second Semester  [CBCS]'),
(909, 'M.Tech. Second Semester  VLSI'),
(910, 'M.Tech. Second Semester (C.A.D./C.A.M.)'),
(911, 'M.Tech. Second Semester (Chemical Engineering)'),
(912, 'M.Tech. Second Semester (Chemical Technology)(Food Technology)'),
(913, 'M.Tech. Second Semester (Chemical Technology)(Petro Chemical Technology)'),
(914, 'M.Tech. Second Semester (Computer Science &amp; Engineering)'),
(915, 'M.Tech. Second Semester (Electronics Engineering Communication) '),
(916, 'M.Tech. Second Semester (Electronics Engineering) '),
(917, 'M.Tech. Second Semester (Heat Power Engineering)'),
(918, 'M.Tech. Second Semester (Integrated Power System)'),
(919, 'M.Tech. Second Semester (Mechanical Engineering Design)'),
(920, 'M.Tech. Second Semester (Power Electronics And Power System)'),
(921, 'M.Tech. Second Semester (Structural Engineering)'),
(922, 'M.Tech. Second Semester of Software System [CBS]'),
(923, 'M.Tech. Third &amp; Final Semester (Computer Science &amp; Engineering) (Winter 2016)'),
(924, 'M.Tech. Third Semester (C.A.D / C.A.M)'),
(925, 'M.Tech. Third Semester (Heat Power Engineering)'),
(926, 'M.Tech. Third Semester (Integrated Power System)'),
(927, 'M.Tech. Third Semester (Power Electronics And Power System)'),
(928, 'M.Tech. Third Semester Electronics Engg.'),
(929, 'M.Tech. Third Semester Electronics Engg. (Communication)'),
(930, 'M.Tech. Third Semester Industrial Engg. (Summer 2016)'),
(931, 'M.Tech. Third Semester of Software System [CBS]'),
(932, 'M.Tech. Third Semester Structural Engg.'),
(933, 'M.Tech. Third Semester VLSI'),
(934, 'Master of Computer Application Part-I (Semester-I)'),
(935, 'Master of Computer Application Part-I (Semester-II)'),
(936, 'Master of Computer Application Part-II (Semester-I)'),
(937, 'Master of Computer Application Part-II (Semester-II)'),
(938, 'Master of Computer Application Part-III (Semester-I)'),
(939, 'Master of Computer Application Part-III (Semester-II)'),
(940, 'Master of Pharmacy  Fourth Semester  [CBS] PHARMACEUTICAL CHEMISTRY'),
(941, 'Master of Pharmacy  Fourth Semester  [CBS] PHARMACEUTICS'),
(942, 'Master of Pharmacy  Fourth Semester  [CBS] PHARMACOGNOSY'),
(943, 'Master of Pharmacy  Fourth Semester  [CBS] PHARMACOLOGY'),
(944, 'Master of Pharmacy  Fourth Semester  [CBS] QUALITY ASSURANCE'),
(945, 'Master of Pharmacy First Semester  [CBS] PHARMACEUTICAL CHEMISTRY'),
(946, 'Master of Pharmacy First Semester  [CBS] PHARMACEUTICS'),
(947, 'Master of Pharmacy First Semester  [CBS] PHARMACOGNOSY'),
(948, 'Master of Pharmacy First Semester  [CBS] PHARMACOLOGY'),
(949, 'Master of Pharmacy First Semester  [CBS] QUALITY ASSURANCE'),
(950, 'Master of Pharmacy Second Semester  [CBS] PHARMACEUTICAL CHEMISTRY'),
(951, 'Master of Pharmacy Second Semester  [CBS] PHARMACEUTICS'),
(952, 'Master of Pharmacy Second Semester  [CBS] PHARMACOGNOSY'),
(953, 'Master of Pharmacy Second Semester  [CBS] PHARMACOLOGY'),
(954, 'Master of Pharmacy Second Semester  [CBS] QUALITY ASSURANCE'),
(955, 'Master of Pharmacy Third Semester  [CBS]  PHARMACEUTICAL CHEMISTRY'),
(956, 'Master of Pharmacy Third Semester  [CBS]  PHARMACEUTICS'),
(957, 'Master of Pharmacy Third Semester  [CBS]  PHARMACOGNOSY'),
(958, 'Master of Pharmacy Third Semester  [CBS]  PHARMACOLOGY'),
(959, 'Master of Pharmacy Third Semester  [CBS]  QUALITY ASSURANCE'),
(960, 'P.G. Dip. in Digital &amp; Cyber Forensic And Related '),
(961, 'P.G. Dip. in Forensic Science And Related law'),
(962, 'P.G. Dip. in Nanoscience &amp; Nanotechnology'),
(963, 'P.G. Dip. in Nanotechnology'),
(964, 'P.G. Dip. in Ornithology'),
(965, 'P.G. Diploma in Industrial Robotics Second Semester [CBS]'),
(966, 'Post B.Sc. Diploma In Computer Science &amp; Application Second Semester [CBS]');

-- --------------------------------------------------------

--
-- Table structure for table `notify`
--

CREATE TABLE IF NOT EXISTS `notify` (
  `id` int(11) NOT NULL,
  `email` varchar(150) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notify`
--

INSERT INTO `notify` (`id`, `email`, `created`) VALUES
(1, 'as@gmail.com', '2017-09-14 09:54:00'),
(2, 'h@gmail.com', '2017-09-14 09:55:24'),
(3, 't@gmail.com', '2017-09-14 09:55:46'),
(4, 'q@gmail.com', '2017-09-14 09:56:18'),
(5, 'rt@gmail.com', '2017-09-16 10:03:27');

-- --------------------------------------------------------

--
-- Table structure for table `question_paper_uploads`
--

CREATE TABLE IF NOT EXISTS `question_paper_uploads` (
  `id` int(11) NOT NULL,
  `university_id` int(11) NOT NULL,
  `course_id` varchar(200) NOT NULL,
  `paper_year` varchar(11) NOT NULL,
  `season` varchar(10) NOT NULL,
  `subject_name` varchar(120) NOT NULL,
  `file_path` varchar(300) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question_paper_uploads`
--

INSERT INTO `question_paper_uploads` (`id`, `university_id`, `course_id`, `paper_year`, `season`, `subject_name`, `file_path`, `created`, `modified`) VALUES
(8, 1, 'B. Arch. (I.D.) Eighth Semester ', '2011', '0', 'science', 'file/question_paper/2017/9198622016-11-08 (1).jpg', '2017-09-15 10:27:04', '2017-09-15 10:27:04'),
(9, 1, 'B. Arch. (I.D.) Ninth Semester ', '2010', '0', 'w', 'file/question_paper/2017/8688071.jpg', '2017-09-15 11:39:51', '2017-09-15 11:39:51'),
(13, 1, 'B.E. Seventh Semester (Computer Science & Engineering)', '2011', '1', 'tom', 'file/question_paper/2017/4047601 (1).jpg', '2017-09-18 07:03:58', '2017-09-18 07:03:58');

-- --------------------------------------------------------

--
-- Table structure for table `universities`
--

CREATE TABLE IF NOT EXISTS `universities` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `universities`
--

INSERT INTO `universities` (`id`, `name`, `description`, `created`) VALUES
(1, 'Rashtrasant Tukadoji Maharaj Nagpur University', 'maharashtra university ', '2017-05-19 11:52:55'),
(5, 'gadgebaba', 'amrawati university', '2017-05-19 14:39:15'),
(7, 'Amravati University', 'Near Nagpur', '2017-05-20 06:40:38'),
(8, 'New test university', 'test after new code dk', '2017-05-20 06:58:38'),
(9, 'punjab', 'punjabuniversity', '2017-09-15 08:10:42');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `type_id` int(11) DEFAULT NULL,
  `fname` varchar(127) DEFAULT NULL,
  `lname` varchar(127) DEFAULT NULL,
  `mobile` varchar(15) DEFAULT NULL,
  `otp` varchar(7) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `tution_detail_status` tinyint(1) DEFAULT '0',
  `email` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `type_id`, `fname`, `lname`, `mobile`, `otp`, `is_active`, `tution_detail_status`, `email`, `username`, `password`, `created`, `modified`) VALUES
(13, 3, 'Harish', 'Chopkar', '7387990061', NULL, NULL, 0, 'harish@gmail.com', 'Harish', '$2y$10$y4g37wvIBrnEuRGYeWTFp.OOMrgpxQqcFIcIiecfOhliuhly7QiJy', '2017-09-14 11:11:01', '2017-09-14 11:11:01');

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE IF NOT EXISTS `user_types` (
  `id` int(11) NOT NULL,
  `type_name` varchar(64) DEFAULT NULL,
  `description` varchar(127) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`id`, `type_name`, `description`, `created_date`) VALUES
(1, 'student', 'normal student', NULL),
(2, 'tution', 'organisation', NULL),
(3, 'admin', 'main admin', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nagpur_university_courses`
--
ALTER TABLE `nagpur_university_courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notify`
--
ALTER TABLE `notify`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question_paper_uploads`
--
ALTER TABLE `question_paper_uploads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `universities`
--
ALTER TABLE `universities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_types`
--
ALTER TABLE `user_types`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `nagpur_university_courses`
--
ALTER TABLE `nagpur_university_courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=967;
--
-- AUTO_INCREMENT for table `notify`
--
ALTER TABLE `notify`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `question_paper_uploads`
--
ALTER TABLE `question_paper_uploads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `universities`
--
ALTER TABLE `universities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `user_types`
--
ALTER TABLE `user_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
