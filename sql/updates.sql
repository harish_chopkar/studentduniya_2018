--
-- 10-04-2018
--

CREATE TABLE `subjects` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(255) COLLATE 'utf8_general_ci' NOT NULL
);

INSERT INTO `subjects` (`name`) VALUES ('Mathematics');
INSERT INTO `subjects` (`name`) VALUES ('Physics');
INSERT INTO `subjects` (`name`) VALUES ('Chemistry');

ALTER TABLE `nagpur_university_courses` ADD `university_id` int(11) NULL AFTER `id`,
CHANGE `course_name` `course_name` varchar(150) COLLATE 'utf8_general_ci' NOT NULL AFTER `university_id`;
ALTER TABLE `nagpur_university_courses` ADD `university_type_id` int NULL;
ALTER TABLE `nagpur_university_courses` ADD `tution_id` int(11) NULL;

UPDATE nagpur_university_courses SET university_id = '1';
UPDATE nagpur_university_courses SET university_type_id = '1';

ALTER TABLE `nagpur_university_courses`ADD `created` datetime NULL, ADD `modified` datetime NULL AFTER `created`;

CREATE TABLE `course_subjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `user_id` int(11) NULL,
  `tution_id` int(11) NULL,
  `course_id` int(11) NULL,
  `subject_name` varchar(255) COLLATE 'utf8_general_ci' NULL,
  `created` datetime NULL,
  `modified` datetime NULL
);

ALTER TABLE `tutions` ADD `parent_id` int(11) NULL AFTER `id`;

ALTER TABLE `tutions`
CHANGE `tution_description` `tution_description` varchar(300) COLLATE 'latin1_swedish_ci' NULL AFTER `tution_name`,
CHANGE `email` `email` varchar(250) COLLATE 'latin1_swedish_ci' NULL AFTER `owner_name`,
CHANGE `district` `district` varchar(100) COLLATE 'latin1_swedish_ci' NULL AFTER `street_address`,
CHANGE `state` `state` varchar(100) COLLATE 'latin1_swedish_ci' NULL AFTER `district`,
CHANGE `zip_code` `zip_code` int(7) NULL AFTER `state`,
CHANGE `country` `country` varchar(100) COLLATE 'latin1_swedish_ci' NULL DEFAULT 'India' AFTER `zip_code`,
CHANGE `tag_line` `tag_line` varchar(300) COLLATE 'latin1_swedish_ci' NULL AFTER `contact`,
CHANGE `cover_photo` `cover_photo` varchar(300) COLLATE 'latin1_swedish_ci' NULL AFTER `tag_line`,
CHANGE `modified` `modified` datetime NULL AFTER `created`;

--
-- 01-05-2018
--

CREATE TABLE `courses` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `course_name` varchar(255) NULL,
  `university_type_id` int NULL,
  `fees` int NULL,
  `tution_id` int NULL,
  `created` datetime NULL,
  `modified` datetime NULL
) COLLATE 'utf8_general_ci';

ALTER TABLE `courses`
ADD `start_date` datetime NULL AFTER `tution_id`;