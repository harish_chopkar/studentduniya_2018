<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Admins Controller
 *
 *
 * @method \App\Model\Entity\Admin[] paginate($object = null, array $settings = [])
 */
class AdminsController extends AppController
{

//The first method which is executed first
    public function isAuthorized($user)
    {
        if($user['type_id'] == 3){
            return true;
        }
        return parent::isAuthorized($user);
    }

    public function initialize(){
        parent::initialize();
        $this->viewBuilder()->setLayout('sdadmin');
        $this->loadModel('Users');
    }

    public function index(){
    }
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['UserTypes']
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }


    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function viewTutionUser()
    {
        $tutionusers = $this->paginate($this->Users->findByType_id(2));
        $this->set(compact('tutionusers'));
        $this->set('_serialize', ['tutionusers']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */

/**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function viewTutionDetails()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $tutions = $this->paginate($this->Tutions);

        $this->set(compact('tutions'));
        $this->set('_serialize', ['tutions']);
    }
    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function addTutionUser()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $types = $this->Users->UserTypes->find('list', ['limit' => 200]);
        $this->set(compact('user', 'types'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user detail has been updated.'));

                return $this->redirect(['action' => 'viewTutionUser']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $types = $this->Users->UserTypes->find('list', ['limit' => 200]);
        $this->set(compact('user', 'types'));
        $this->set('_serialize', ['user']);
    }

    public function adminedit()
    {
        $id = $this->Auth->User('id');

        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Admin Profile has been updated.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
