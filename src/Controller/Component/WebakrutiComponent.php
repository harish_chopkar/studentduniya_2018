<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;

/**
 * Webakruti component
 *	   This component can be loaded in Action or in Intialise method
 *     $this->loadComponent('Webakruti');
 *     $this->Webakruti->actionName();
 */
class WebakrutiComponent extends Component
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    //This code is used to Upload the Data as File
    public function imageupload($filedata = Array(), $type){
        if(!empty($filedata)){
            $file = $filedata;
            $randomName = rand(10,1000000);
            if (!file_exists(WWW_ROOT . 'file/'.$type.'/' . date("Y"))){
                mkdir(WWW_ROOT . 'file/'.$type.'/' . date("Y"), 0777, true);
            }
            $target_file = WWW_ROOT . 'file/'.$type.'/' . date("Y") . '/' . basename($file['name']);
            $fileSize = $file['size'];
            if($fileSize > 8000000){
                return false;
            }else{
                $fileFleTyp = pathinfo($target_file,PATHINFO_EXTENSION);
                $fileFleTyp = strtolower($fileFleTyp);
                if($fileFleTyp != "jpg" && $fileFleTyp != "png" && $fileFleTyp != "jpeg" && $fileFleTyp != "gif" && $fileFleTyp != "pdf" ){
                    return false;
                }else{
                    $target_file_withNewName = WWW_ROOT . 'file/'.$type.'/'. date("Y") . '/' . $randomName . $file['name']; 
                    if(move_uploaded_file($file['tmp_name'], $target_file_withNewName)){
                       return 'file/'.$type.'/'. date("Y") . '/' . $randomName . $file['name'];
                    }else{
                        return false;
                    }
                }    
            }       
        }
        return false;
    }


    //code to download the File
    public function download($id=null) { 
        $this->request->allowMethod(['post']);
        $id = $this->request->getData('id');
        $response = $this->response->withFile($id,['download' => true]);
        // Return the response to prevent controller from trying to render
        // a view.
        return $response;
    }
    
}
