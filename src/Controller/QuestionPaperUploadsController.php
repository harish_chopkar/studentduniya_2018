<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * QuestionPaperUploads Controller
 *
 * @property \App\Model\Table\QuestionPaperUploadsTable $QuestionPaperUploads
 *
 * @method \App\Model\Entity\QuestionPaperUpload[] paginate($object = null, array $settings = [])
 */
class QuestionPaperUploadsController extends AppController
{


    //The first method which is executed first
    public function isAuthorized($user)
    {
        if($user['type_id'] == 3){
            return true;
        }
        return parent::isAuthorized($user);
    }

    public function initialize(){
        parent::initialize();
        $this->viewBuilder()->setLayout('sdadmin');
        $this->loadModel('NagpurUniversityCourses');
    }


    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Universities', 'NagpurUniversityCourses']
        ];
        $questionPaperUploads = $this->paginate($this->QuestionPaperUploads);

        $this->set(compact('questionPaperUploads'));
        $this->set('_serialize', ['questionPaperUploads']);
    }


    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $questionPaperUpload = $this->QuestionPaperUploads->newEntity();
        $courses = $this->NagpurUniversityCourses->find('list', [
            'keyField' => 'id',
            'valueField' => 'course_name'
        ]);
        if ($this->request->is('post')) {
            $file_path =  $this->imageupload($this->request->data['file_path'], 'question_paper');
            if(!empty($file_path)){
            $this->request->data['file_path']=$file_path;
            $questionPaperUpload = $this->QuestionPaperUploads->patchEntity($questionPaperUpload, $this->request->getData());

            if ($this->QuestionPaperUploads->save($questionPaperUpload)) {
                $this->Flash->success(__('The question paper upload has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
        }
            $this->Flash->error(__('The question paper upload could not be saved. Please, try again.'));
        }
        $universities = $this->QuestionPaperUploads->Universities->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ]);

        $this->set(compact('questionPaperUpload', 'universities', 'courses'));
        $this->set('_serialize', ['questionPaperUpload']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Question Paper Upload id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $questionPaperUpload = $this->QuestionPaperUploads->get($id, [
            'contain' => []
        ]);
        $courses = $this->NagpurUniversityCourses->find('list', [
            'keyField' => 'id',
            'valueField' => 'course_name'
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $questionPaperUpload = $this->QuestionPaperUploads->patchEntity($questionPaperUpload, $this->request->getData());
            if ($this->QuestionPaperUploads->save($questionPaperUpload)) {
                $this->Flash->success(__('The question paper upload has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The question paper upload could not be saved. Please, try again.'));
        }
        $universities = $this->QuestionPaperUploads->Universities->find('list', ['limit' => 200]);
        $this->set(compact('questionPaperUpload', 'universities','courses'));
        $this->set('_serialize', ['questionPaperUpload']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Question Paper Upload id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $questionPaperUpload = $this->QuestionPaperUploads->get($id);
        if ($this->QuestionPaperUploads->delete($questionPaperUpload)) {
            $this->Flash->success(__('The question paper upload has been deleted.'));
        } else {
            $this->Flash->error(__('The question paper upload could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    //code to download the File
    public function download($id=null) {
        $this->request->allowMethod(['post']);
        $id = $this->request->getData('id');
        $response = $this->response->withFile($id,['download' => true]);
        // Return the response to prevent controller from trying to render
        // a view.
        return $response;
    }


//This code is used to Upload the Data as File
    protected function imageupload($filedata = Array(), $type){
        if(!empty($filedata)){
            $file = $filedata;
            $randomName = rand(10,1000000);
            if (!file_exists(WWW_ROOT . 'file/'.$type.'/' . date("Y"))){
                mkdir(WWW_ROOT . 'file/'.$type.'/' . date("Y"), 0777, true);
            }
            $target_file = WWW_ROOT . 'file/'.$type.'/' . date("Y") . '/' . basename($file['name']);
            $fileSize = $file['size'];
            if($fileSize > 8000000){
                return false;
            }else{
                $fileFleTyp = pathinfo($target_file,PATHINFO_EXTENSION);
                $fileFleTyp = strtolower($fileFleTyp);
                if($fileFleTyp != "jpg" && $fileFleTyp != "png" && $fileFleTyp != "jpeg" && $fileFleTyp != "gif" && $fileFleTyp != "pdf" ){
                    return false;
                }else{
                    $target_file_withNewName = WWW_ROOT . 'file/'.$type.'/'. date("Y") . '/' . $randomName . $file['name'];
                    if(move_uploaded_file($file['tmp_name'], $target_file_withNewName)){
                       return 'file/'.$type.'/'. date("Y") . '/' . $randomName . $file['name'];
                    }else{
                        return false;
                    }
                }
            }
        }
        return false;
    }

}
