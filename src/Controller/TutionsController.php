<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Tutions Controller
 *
 * @property \App\Model\Table\TutionsTable $Tutions
 *
 * @method \App\Model\Entity\Tution[] paginate($object = null, array $settings = [])
 */
class TutionsController extends AppController
{

//The first method which is executed first
    public function isAuthorized($user)
    {
        if($user['type_id'] == 2){
            return true;
        }
        return parent::isAuthorized($user);
    }

    public function initialize(){
        parent::initialize();
        $this->viewBuilder()->setLayout('tution');
        $this->loadModel('Users');
        $this->loadModel('States');
        $this->loadModel('Districts');
        $this->loadModel('TutionCourses');
        $this->loadModel('CourseSubjects');
        //Custom components
        $this->loadComponent('Webakruti');

    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $tutions = $this->Tutions->findByUser_id($this->Auth->User('id'))
        ->contain(['Districts'])
        ->order(['Tutions.id']);
        $this->set(compact('tutions'));
        $this->set('_serialize', ['tutions']);
    }

    /**
     * View method
     *
     * @param string|null $id Tution id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    // public function view($id = null)
    // {

    //     $tution = $this->Tutions->get($id, [
    //         'contain' => ['Users']
    //     ]);

    //     $this->set('tution', $tution);
    //     $this->set('_serialize', ['tution']);
    // }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $mainBranch = $this->Tutions->find()->where([
                'user_id' => $this->Auth->user('id'),
                'parent_id IS NULL'
            ])->first();
        // if(!empty($tution_user_data))
        // {
        //     $this->Flash->error(__('Your Tution Information already added! Click on Edit to edit Details'));
        //     return $this->redirect(['action' => 'index']);
        // }

        $tution = $this->Tutions->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['user_id'] = $this->Auth->user('id');
            $this->request->data['state'] = 21;
            $this->request->data['country'] = 'India';
            if (!empty($mainBranch->id)) {
                $this->request->data['parent_id'] = $mainBranch->id;
            }
            $tution = $this->Tutions->patchEntity($tution, $this->request->getData());
            if ($this->Tutions->save($tution)) {
                $this->Flash->success(__('The tution has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The tution could not be saved. Please, try again.'));
        }
        $districts = $this->Districts->findByState_id(21);
        $this->set(compact('tution', 'districts', 'mainBranch'));
        $this->set('_serialize', ['tution', 'mainBranch']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tution id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {

        // $file_path =  $this->imageupload($this->request->data['file_path'], 'question_paper');

        $tution = $this->Tutions->get($id, [
            'contain' => []
        ]);
        if($tution->user_id != $this->Auth->User('id')){
            $this->Flash->error(__('You are not authorized to edit different page!'));
            return $this->redirect(['action' => 'index']);
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tution = $this->Tutions->patchEntity($tution, $this->request->getData());
            if ($this->Tutions->save($tution)) {
                $this->Flash->success(__('The tution has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The tution could not be saved. Please, try again.'));
        }
        $users = $this->Tutions->Users->find('list', ['limit' => 200]);
        $districts = $this->Districts->findByState_id(21);
        $this->set(compact('tution','users','districts'));
        $this->set('_serialize', ['tution']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tution id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    // public function delete($id = null)
    // {
    //     $this->request->allowMethod(['post', 'delete']);
    //     $tution = $this->Tutions->get($id);
    //     if ($this->Tutions->delete($tution)) {
    //         $this->Flash->success(__('The tution has been deleted.'));
    //     } else {
    //         $this->Flash->error(__('The tution could not be deleted. Please, try again.'));
    //     }

    //     return $this->redirect(['action' => 'index']);
    // }

    // Code for Cover photoUpload
    public function coveradd()
    {   //loads Webakruti Component
        $this->loadComponent('Webakruti');
         $user_id =  $this->Auth->user('id');
         $tution = $this->Tutions->find()
                ->where(['user_id IS' => $user_id, 'parent_id IS NULL'])->first();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $file_path = $this->Webakruti->imageupload($this->request->data['cover_photo'], 'tutioncover');
             $this->Webakruti->imageupload($filedata = Array(), 'cover');
             $this->request->data['cover_photo']=$file_path;
                    $tution = $this->Tutions->patchEntity($tution, $this->request->getData());
                    if ($this->Tutions->save($tution)) {
                        $this->Flash->success(__('The tution has been saved.'));
                         return $this->redirect(['action' => 'index']);
                    }
                    $this->Flash->error(__('The tution could not be saved. Please, try again.'));
                }

         $this->set(compact('tution'));
    }


    /*Method to edit user*/
    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edituser()
    {
        $user = $this->Users->get($this->Auth->User('id'), [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user detail has been updated.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $types = $this->Users->UserTypes->find('list', ['limit' => 200]);
        $this->set(compact('user', 'types'));
        $this->set('_serialize', ['user']);
    }

    public function courses($tutionId = null)
    {
        if (empty($tutionId)) {
            $this->Flash->error(__('Invalid Opration'));
            return $this->redirect($this->referer());
        }
        $tution = $this->Tutions->find()
            ->where(['Tutions.id' => $tutionId])
            ->contain(['TutionCourses', 'TutionCourses.CourseSubjects'])
            ->first();
        if (empty($tution)) {
            $this->Flash->error(__('Invalid Tution Details'));
            return $this->redirect($this->referer());
        }
        $this->set(compact('tution', 'tutionId'));
        $this->set('_serialize', ['tution', 'tutionId']);
    }

    public function addCourses()
    {
        if ($this->request->is(['post', 'put'])) {
            $data = $this->request->data;
            if(
                empty($data['tution_id']) ||
                empty($data['course_name']) ||
                empty($data['subject_name'])
            ) {
                $this->Flash->error(__('Invalid Course Details'));
                return $this->redirect($this->referer());
            }

            $courseData = [
                'university_type_id' => 4,
                'tution_id' => $data['tution_id'],
                'course_name' => $data['course_name'],
                'fees' => $data['fees'],
                'start_date' =>  !empty($data['start_date']) ? date('Y-m-d H:i:s', strtotime($data['start_date'])) : null
            ];
            $courseEntity = $this->TutionCourses->newEntity();
            $courseEntity = $this->TutionCourses->patchEntity($courseEntity,  $courseData);

            if ($response = $this->TutionCourses->save($courseEntity)) {
                foreach ($data['subject_name'] as $key => $value) {
                    $subjectData = [
                        'tution_id' => $data['tution_id'],
                        'course_id' => $response->id,
                        'subject_name' => $value,
                    ];

                    $subjectEntity = $this->CourseSubjects->newEntity();
                    $subjectEntity = $this->CourseSubjects->patchEntity($subjectEntity, $subjectData);
                    $this->CourseSubjects->save($subjectEntity);
                }
                $this->Flash->success(__('Course Added '));
            } else {
                $this->Flash->success(__('Unable to add course details '));
            }

        } else {
            $this->Flash->error(__('Invalid Operation'));
        }

        return $this->redirect($this->referer());
    }

    public function deleteTutionCourse($courseId = null)
    {
        $response = ['success' => false, 'message' => '', 'error' => ''];

        if (!empty($courseId)) {
            if ($this->TutionCourses->deleteAll(['TutionCourses.id' => $courseId])) {
                $this->CourseSubjects->deleteAll(['CourseSubjects.course_id' => $courseId]);
                $response['success'] = true;
            }
        }
        $this->set('response', $response);
        $this->set('_serialize', ['response']);
    }

    public function editTutionCourseDetails($courseId) {
        $this->viewBuilder()->setLayout(false);
        $course = $this->TutionCourses->find()
            ->where(['TutionCourses.id' => $courseId])
            ->contain(['CourseSubjects'])
            ->first();
        $this->set('course', $course);
        $this->set('_serialize', ['course']);
    }

    public function editCourses() {
        if ($this->request->is('post')) {
            $data = $this->request->data;

            $dataToSave = [
                'id' => $data['course_id'],
                'course_name' => $data['course_name'],
                'tution_d' => $data['tution_id'],
                'university_type_id' => 4,
                'fees' => $data['fees'],
                'start_date' => !empty($data['start_date']) ? date('Y-m-d H:i:s', strtotime($data['start_date'])) : null
            ];

            foreach ($data['subject_name'] as $key => $subjectName) {
                $dataToSave['course_subjects'][$key] = ['subject_name' => $subjectName, 'tution_id' => $data['tution_id']];
            }
            if (!empty($data['subject_id'])) {
                $deleteIds = $this->CourseSubjects->find('list', [
                    'conditions' => [
                        'course_id' => $data['course_id'],
                        'id NOT IN ' => $data['subject_id']
                    ],
                    'keyField' => 'id',
                    'valueField' => 'id'
                ]);
                if (!empty($deleteIds) && !empty($deleteIds->toArray())) {
                    $deleteIds =  $deleteIds->toArray();
                }
            }

            $courseEntity = $this->TutionCourses->newEntity();
            $courseEntity =  $this->TutionCourses->patchEntity($courseEntity, $dataToSave);
            $courseEntity->id = $data['course_id'];

            foreach ($courseEntity->course_subjects as $key => $entity) {
                if (isset($data['subject_id'][$key])) {
                    $courseEntity->course_subjects[$key]['id'] = $data['subject_id'][$key];
                }
            }

            if($this->TutionCourses->save($courseEntity)) {

                if (!empty($deleteIds) && !empty($deleteIds->toArray())) {
                    $this->CourseSubjects->deleteAll(['id IN ' => $deleteIds->toArray()]);
                }
                $this->Flash->success(__('Course Updated '));
            } else {
                $this->Flash->error(__('Unable to updated the course'));
            }
        }
        $this->redirect($this->referer());
    }

}
