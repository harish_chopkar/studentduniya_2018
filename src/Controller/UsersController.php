<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    public function initialize(){
        parent::initialize();
        $this->Auth->allow(['searchpaper','download','logout','index', 'about', 'contact','viewpaper', 'viewTutions','viewTuitionDetails']);
        $this->viewBuilder()->layout('sdcommon');
        $this->loadModel('ContactUs');
        $this->loadModel('Universities');
        $this->loadModel('Tutions');
        $this->loadModel('NagpurUniversityCourses');
        $this->loadModel('QuestionPaperUploads');
        $this->loadModel('Notify');
        $this->loadComponent('Flash');
        $this->loadComponent('Csrf');
    }

    public function login(){
        $this->viewBuilder()->layout('sdlogin');
        if ($this->request->is('post')) {
             $user = $this->Auth->identify();
                if ($user) {
                    $this->Auth->setUser($user);
                    if($user['type_id']==3){
                        $this->redirect(['controller'=>'Admins', 'action' => 'index']);
                    }
                    if($user['type_id']==2){
                        $this->redirect(['controller'=>'Tutions', 'action' => 'index']);
                    }
                    return $this->redirect($this->Auth->redirectUrl());
                 }
                    $this->Flash->error('Your username or password is incorrect.');
            }
    }
    public function logout()
        {
            $this->Flash->success('You are now logged out.');
            return $this->redirect($this->Auth->logout());
        }


    public function index(){
          $notify = $this->Notify->newEntity();
          if ($this->request->is('post')) {
            $notify = $this->Notify->patchEntity($notify, $this->request->getData());
            if ($this->Notify->save($notify)) {
                $this->Flash->success(__('Data has been Submitted.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Please, try again.'));
        }
         $this->set(compact('notify'));
    }

    public function about(){
    }

    public function contact(){
          $contactUs = $this->ContactUs->newEntity();
          if ($this->request->is('post')) {
            $contactUs = $this->ContactUs->patchEntity($contactUs, $this->request->getData());
            if ($this->ContactUs->save($contactUs)) {
                $this->Flash->success(__('Data has been Submitted.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Please, try again.'));
        }
           $this->set(compact('contactUs'));
    }

//register new user or any type of admin
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $types = $this->Users->UserTypes->find('list', ['limit' => 200]);
        $this->set(compact('user', 'types'));
        $this->set('_serialize', ['user']);
    }


    public function viewpaper(){
        $id=1;
        $courses = $this->NagpurUniversityCourses->find('list', [
            'keyField' => 'id',
            'valueField' => 'course_name'
        ]);

        $university = $this->QuestionPaperUploads->find('all')
        ->contain(['Universities', 'NagpurUniversityCourses'])
        ->order('QuestionPaperUploads.created DESC')
        ->limit(20);

        // debug($university->toArray());exit();
        $this->set(compact('university', 'courses'));
        $this->set('_serialize', ['university']);
    }

    public function searchpaper(){
        if($this->request->is(['post'])){
         $id=1;
        $courses = $this->NagpurUniversityCourses->find('list', [
            'keyField' => 'id',
            'valueField' => 'course_name'
        ]);
         $course_id = $this->request->getData('course_id');
         $paper_year = $this->request->getData('paper_year');
         $year = array(2010,2011,2012,2013,2014,2015,2016,2017);
         if(!(in_array($paper_year, $year))){
            $this->Flash->error(__('Invalid paper year entered.'));
            return $this->redirect(['action' => 'viewpaper']);
         }
          $questions_paper = $this->QuestionPaperUploads->find('all',[
                'conditions'=>['QuestionPaperUploads.university_id'=>$id,'QuestionPaperUploads.course_id'=>$course_id,'QuestionPaperUploads.paper_year'=>$paper_year],
                'contain'=>['Universities']
            ]);
        $this->set(compact('questions_paper', 'courses'));
        $this->set('_serialize', ['questions_paper']);
        }
        else{
            return $this->redirect(['action' => 'viewpaper']);
        }
    }

    //code to download the File
    public function download($id=null) {
        $this->request->allowMethod(['post']);
        $id = $this->request->getData('id');
        $response = $this->response->withFile($id,['download' => true]);
        // Return the response to prevent controller from trying to render
        // a view.
        return $response;
    }

    // Tution MOdules to View Tution Details

    public function viewTutions(){
        $keyword = '';
        $contain = false;
        $options = [
            'conditions' => [
                'Tutions.parent_id IS NULL'
            ],
            'contain' => $contain,
            'order' => ['Tutions.id DESC']
        ];

        if (!empty($this->request->query['subject'])) {
            $keyword = $this->request->query['subject'];
        }

        if (!empty($keyword)) {
            $tutionData =  $this->Tutions->find('all', [
                'join' => [
                    'TutionSubject' => [
                        'table' => 'course_subjects',
                        'type' => 'LEFT',
                        'conditions' => 'TutionSubject.tution_id = Tutions.id',
                    ],
                    'ChildTution' => [
                        'table' => 'tutions',
                        'type' => 'LEFT',
                        'conditions' => 'ChildTution.parent_id = Tutions.id',
                    ],
                    'ChildTutionSubject' => [
                        'table' => 'course_subjects',
                        'type' => 'LEFT',
                        'conditions' => 'ChildTutionSubject.tution_id = ChildTution.id',
                    ]
                ],
                'conditions' => [
                    'OR' => [
                        'TutionSubject.subject_name LIKE "%' . $keyword . '%"',
                        'ChildTutionSubject.subject_name LIKE "%' . $keyword . '%"',
                    ],
                    'Tutions.parent_id IS NULL'
                ],
                'group' => 'Tutions.id'
            ]);
        } else {
            $tutionData = $this->Tutions->find('all', $options);
        }
        $this->set(compact('tutionData', 'keyword'));

    }

    public function viewTuitionDetails($id = NULL)
    {
        $tutions = $this->Tutions->find('all', [
            'conditions' => [
                'OR' => [
                    'Tutions.id' => $id,
                    'Tutions.parent_id' => $id
                ]
            ],
            'contain' => ['Districts', 'TutionCourses', 'TutionCourses.CourseSubjects'],
            'order' => ['Tutions.id']
        ] );
        // debug($tutions->toArray());exit();
        $this->set(compact('tutions'));
        $this->set('_serialize', ['tutions']);
    }


}






















