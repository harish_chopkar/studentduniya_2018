<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Notify Entity
 *
 * @property int $id
 * @property string $email
 * @property \Cake\I18n\FrozenTime $created
 */
class Notify extends Entity
{

}
