<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * QuestionPaperUpload Entity
 *
 * @property int $id
 * @property int $university_id
 * @property string $course_id
 * @property string $paper_year
 * @property string $season
 * @property string $subject_name
 * @property string $file_path
 * @property \Cake\I18n\FrozenTime $created
 *
 * @property \App\Model\Entity\University $university
 * @property \App\Model\Entity\Course $course
 */
class QuestionPaperUpload extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
