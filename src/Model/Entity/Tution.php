<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Tution Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $tution_name
 * @property string $tution_description
 * @property string $owner_name
 * @property string $email
 * @property string $started_in
 * @property string $street_address
 * @property string $district
 * @property string $state
 * @property int $zip_code
 * @property string $country
 * @property string $landmark
 * @property string $contact
 * @property string $tag_line
 * @property string $cover_photo
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\User $user
 */
class Tution extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
