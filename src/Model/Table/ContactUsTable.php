<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ContactUs Model
 *
 * @method \App\Model\Entity\ContactU get($primaryKey, $options = [])
 * @method \App\Model\Entity\ContactU newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ContactU[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ContactU|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ContactU patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ContactU[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ContactU findOrCreate($search, callable $callback = null, $options = [])
 */
class ContactUsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('contact_us');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->add('fname',[
                'minLength' => [
                    'rule' => ['minLength', 2],
                    'last' => true,
                    'message' => 'Please Enter Name Properly.'
                ],
                'maxLength' => [
                    'rule' => ['maxLength', 30],
                    'message' => 'First Name cannot be too long.'
                ]
            ]) 
            ->scalar('fname')
            ->requirePresence('fname', 'create')
            ->notEmpty('fname');

        $validator
            ->add('lname',[
                'minLength' => [
                    'rule' => ['minLength', 2],
                    'last' => true,
                    'message' => 'Please Enter Name Properly.'
                ],
                'maxLength' => [
                    'rule' => ['maxLength', 30],
                    'message' => 'Last Name cannot be too long.'
                ]
            ])
            ->scalar('lname')
            ->requirePresence('lname', 'create')
            ->notEmpty('lname');

        $validator
        ->add('education',[
                'minLength' => [
                    'rule' => ['minLength', 1],
                    'last' => true,
                    'message' => 'Please Enter Name Properly.'
                ],
                'maxLength' => [
                    'rule' => ['maxLength', 50],
                    'message' => 'Last Name cannot be too long.'
                ]
            ])
            ->scalar('education')
            ->requirePresence('education', 'create')
            ->notEmpty('education');

        $validator
            ->add('college_name',[
                'minLength' => [
                    'rule' => ['minLength', 1],
                    'last' => true,
                    'message' => 'Please Enter College name Properly.'
                ],
                'maxLength' => [
                    'rule' => ['maxLength', 50],
                    'message' => 'College Name cannot be too long.'
                ]
            ])
            ->scalar('college_name')
            ->requirePresence('college_name', 'create')
            ->notEmpty('college_name');

        $validator
        ->add('email',[
                'minLength' => [
                    'rule' => ['minLength', 5],
                    'last' => true,
                    'message' => 'Please Enter Email Properly.'
                ],
                'maxLength' => [
                    'rule' => ['maxLength', 150],
                    'message' => 'Email cannot be too long.'
                ]
            ])
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
        ->add('mobile',[
                'minLength' => [
                    'rule' => ['minLength', 10],
                    'last' => true,
                    'message' => 'Please Enter Mobile number Properly.'
                ],
                'maxLength' => [
                    'rule' => ['maxLength', 15],
                    'message' => 'Mobile number cannot be too long.'
                ]
            ])
            ->scalar('mobile')
            ->numeric('mobile', 'Enter 10 digit number only!')
            ->requirePresence('mobile', 'create')
            ->notEmpty('mobile');

        $validator
        ->add('message',[
                'minLength' => [
                    'rule' => ['minLength', 5],
                    'last' => true,
                    'message' => 'Minimum 5 characters and numbers are allowed.'
                ],
                'maxLength' => [
                    'rule' => ['maxLength', 300],
                    'message' => 'Message cannot be too long.'
                ]
            ])
            ->alphaNumeric('message', 'Alphabet and Numbers are allowed')
            ->scalar('message')
            ->requirePresence('message', 'create')
            ->notEmpty('message');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->isUnique(['mobile']));

        return $rules;
    }
}
