<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CourseSubjects Model
 *
 * @method \App\Model\Entity\CourseSubject get($primaryKey, $options = [])
 * @method \App\Model\Entity\CourseSubject newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CourseSubject[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CourseSubject|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CourseSubject patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CourseSubject[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CourseSubject findOrCreate($search, callable $callback = null, $options = [])
 */
class CourseSubjectsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('course_subjects');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('subject_name')
            ->requirePresence('subject_name', 'create')
            ->notEmpty('subject_name');

        return $validator;
    }
}
