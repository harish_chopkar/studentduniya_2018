<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * NagpurUniversityCourses Model
 *
 * @method \App\Model\Entity\NagpurUniversityCourse get($primaryKey, $options = [])
 * @method \App\Model\Entity\NagpurUniversityCourse newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\NagpurUniversityCourse[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\NagpurUniversityCourse|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\NagpurUniversityCourse patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\NagpurUniversityCourse[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\NagpurUniversityCourse findOrCreate($search, callable $callback = null, $options = [])
 */
class NagpurUniversityCoursesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('nagpur_university_courses');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('course_name')
            ->requirePresence('course_name', 'create')
            ->notEmpty('course_name');

        return $validator;
    }
}
