<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * QuestionPaperUploads Model
 *
 * @property \App\Model\Table\UniversitiesTable|\Cake\ORM\Association\BelongsTo $Universities
 * @property \App\Model\Table\CoursesTable|\Cake\ORM\Association\BelongsTo $Courses
 *
 * @method \App\Model\Entity\QuestionPaperUpload get($primaryKey, $options = [])
 * @method \App\Model\Entity\QuestionPaperUpload newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\QuestionPaperUpload[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\QuestionPaperUpload|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\QuestionPaperUpload patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\QuestionPaperUpload[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\QuestionPaperUpload findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class QuestionPaperUploadsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('question_paper_uploads');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Universities', [
            'foreignKey' => 'university_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('NagpurUniversityCourses', [
            'foreignKey' => 'course_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('paper_year')
            ->requirePresence('paper_year', 'create')
            ->notEmpty('paper_year');

        $validator
            ->scalar('season')
            ->requirePresence('season', 'create')
            ->notEmpty('season');

        $validator
            ->scalar('subject_name')
            ->requirePresence('subject_name', 'create')
            ->notEmpty('subject_name');

        $validator
            ->scalar('file_path')
            ->requirePresence('file_path', 'create')
            ->notEmpty('file_path');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['university_id'], 'Universities'));
        return $rules;
    }
}
