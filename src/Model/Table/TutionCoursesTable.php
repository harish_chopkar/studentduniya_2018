<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TutionCourses Model
 *
 * @method \App\Model\Entity\TutionCourse get($primaryKey, $options = [])
 * @method \App\Model\Entity\TutionCourse newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TutionCourse[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TutionCourse|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TutionCourse patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TutionCourse[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TutionCourse findOrCreate($search, callable $callback = null, $options = [])
 */
class TutionCoursesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('courses');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');

        $this->hasMany('CourseSubjects', [
            'className' => 'CourseSubjects',
            'foreignKey' => 'course_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('course_name')
            ->requirePresence('course_name', 'create')
            ->notEmpty('course_name');

        return $validator;
    }
}
