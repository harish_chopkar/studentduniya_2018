<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Tutions Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Tution get($primaryKey, $options = [])
 * @method \App\Model\Entity\Tution newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Tution[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Tution|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tution patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Tution[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Tution findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TutionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tutions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);

        $this->hasMany('Districts', [
            'foreignKey' => 'id',
            'bindingKey' => 'district'   //created district as a primary column
        ]);

        $this->hasMany('TutionCourses', [
            'className' => 'TutionCourses',
            'foreignKey' => 'tution_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('tution_name')
            ->maxLength('tution_name', 255)
            ->notEmpty('tution_name');

        $validator
            ->scalar('tution_description')
            ->requirePresence('tution_description', 'create')
            ->maxLength('tution_description', 300)
            ->notEmpty('tution_description');

        $validator
            ->scalar('owner_name')
            ->maxLength('owner_name', 50)
            ->notEmpty('owner_name');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->maxLength('email', 250)
            ->notEmpty('email');

        $validator
            ->scalar('street_address')
            ->notEmpty('street_address');

        $validator
            ->scalar('district')
            ->maxLength('district', 60)
            ->requirePresence('district', 'create')
            ->notEmpty('district');

        $validator
            ->scalar('state')
            ->maxLength('state', 60)
            ->requirePresence('state', 'create')
            ->notEmpty('state');

        $validator
            ->integer('zip_code')
            ->naturalNumber('zip_code')
            ->maxLength('zip_code', 6, "Length should be 6 digit")
            ->requirePresence('zip_code', 'create')
            ->notEmpty('zip_code');

        $validator
            ->scalar('country')
            ->requirePresence('country', 'create')
            ->maxLength('country', 60)
            ->notEmpty('country');

        $validator
            ->scalar('landmark')
            ->maxLength('landmark ', 100)
            ->notEmpty('landmark');

        $validator
            ->scalar('contact')
            ->notEmpty('contact');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        // $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
