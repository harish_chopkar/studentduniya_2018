<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="col-md-5">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Edit User') ?></legend>
            <div class="form-group">
                <label>First Name</label>
                <?php echo $this->Form->control('fname',['label'=>false,'placeholder'=>'First Name', 'class'=>'form-control reg-field']);?>
            </div>
            <div class="form-group">
                <label>Last Name</label>
                <?php echo $this->Form->control('lname',['label'=>false,'placeholder'=>'Last Name', 'class'=>'form-control reg-field']);?>
            </div>
            <div class="form-group">
                <label>Mobile NUmber</label>
                <?php echo $this->Form->control('mobile',['label'=>false,'placeholder'=>'Mobile', 'class'=>'form-control reg-field']);?>
            </div>
            <div class="form-group">
                <label>Email</label>
                <?php echo $this->Form->control('email',['label'=>false,'placeholder'=>'Email', 'class'=>'form-control reg-field']);?>
            </div>
            <div class="form-group">
                <label>UserName</label>
                <?php echo $this->Form->control('username',['label'=>false,'placeholder'=>'Username', 'class'=>'form-control reg-field']);?>
            </div>
            <div class="form-group">
                <label>Password</label>
                <?php echo $this->Form->control('password',['label'=>false,'placeholder'=>'Password', 'class'=>'form-control reg-field']);?>
            </div>
    </fieldset>
    <?= $this->Form->button(__('Submit'),['class'=>'btn btn-success btn-block']) ?>
    <?= $this->Form->end() ?>
</div>
