<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Tution[]|\Cake\Collection\CollectionInterface $tutions
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Tution'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="tutions index large-9 medium-8 columns content">
    <h3><?= __('Tutions') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('tution_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('tution_description') ?></th>
                <th scope="col"><?= $this->Paginator->sort('owner_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('started_in') ?></th>
                <th scope="col"><?= $this->Paginator->sort('street_address') ?></th>
                <th scope="col"><?= $this->Paginator->sort('district') ?></th>
                <th scope="col"><?= $this->Paginator->sort('state') ?></th>
                <th scope="col"><?= $this->Paginator->sort('zip_code') ?></th>
                <th scope="col"><?= $this->Paginator->sort('country') ?></th>
                <th scope="col"><?= $this->Paginator->sort('landmark') ?></th>
                <th scope="col"><?= $this->Paginator->sort('contact') ?></th>
                <th scope="col"><?= $this->Paginator->sort('tag_line') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cover_photo') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($tutions as $tution): ?>
            <tr>
                <td><?= $this->Number->format($tution->id) ?></td>
                <td><?= $tution->has('user') ? $this->Html->link($tution->user->id, ['controller' => 'Users', 'action' => 'view', $tution->user->id]) : '' ?></td>
                <td><?= h($tution->tution_name) ?></td>
                <td><?= h($tution->tution_description) ?></td>
                <td><?= h($tution->owner_name) ?></td>
                <td><?= h($tution->email) ?></td>
                <td><?= h($tution->started_in) ?></td>
                <td><?= h($tution->street_address) ?></td>
                <td><?= h($tution->district) ?></td>
                <td><?= h($tution->state) ?></td>
                <td><?= $this->Number->format($tution->zip_code) ?></td>
                <td><?= h($tution->country) ?></td>
                <td><?= h($tution->landmark) ?></td>
                <td><?= h($tution->contact) ?></td>
                <td><?= h($tution->tag_line) ?></td>
                <td><?= h($tution->cover_photo) ?></td>
                <td><?= h($tution->created) ?></td>
                <td><?= h($tution->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $tution->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $tution->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $tution->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tution->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
