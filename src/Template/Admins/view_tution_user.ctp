<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\tutionuser[]|\Cake\Collection\CollectionInterface $tutionusers
  */

?>
    <div class="row">
                  <div class="col-lg-12">
                        <h1 class="page-header">
                           Tution Users Details
                        </h1>
                    </div>
    </div>
<div class="col-md-12">
        <p><?= $this->Html->link(__('Add New Tution User'), ['action' => 'addTutionUser']) ?></p>
</div>
<div class="tutionusers index large-9 medium-8 columns content">
    <h3><?= __('Question Paper Uploads') ?></h3>
    <table cellpadding="0" cellspacing="0" class="table table-hover table-bordered">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('fname') ?></th>
                <th scope="col"><?= $this->Paginator->sort('lname') ?></th>
                <th scope="col"><?= $this->Paginator->sort('mobile') ?></th>
                <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_active') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($tutionusers as $tutionuser): ?>
            <tr>
                <td><?= $this->Number->format($tutionuser->id) ?></td>
                <td><?= h($tutionuser->fname) ?></td>
                <td><?= h($tutionuser->lname) ?></td>
                <td><?= h($tutionuser->mobile) ?></td>
                <td><?= h($tutionuser->email) ?></td>
                <td><?= h($tutionuser->is_active) ?></td>
                <td><?= h($tutionuser->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $tutionuser->id]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
</div>