    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li>                    
                    <?php
                        echo $this->Html->link(
                            $this->Html->tag('i', '', array('class' => 'fa fa-dashboard fa-fw')) . " Dashboard",
                            array('controller' => 'Admins', 'action' => 'index'),
                            array('escape' => false)
                        );
                    ?>
                </li>
                <li>
                    <a href="javascript:void(0)"><i class="fa fa-bar-chart-o fa-fw"></i> Tutions Users<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <?= $this->Html->link('Register Tutions User', ['controller' => 'Admins', 'action' => 'addTutionUser']);?>
                        </li>
                        <li>
                            <?= $this->Html->link('Tution Users Details', ['controller' => 'Admins', 'action' => 'viewTutionUser']);?>
                        </li>
                     
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                <li>
                    <a href="javascript:void(0)"><i class="fa fa-bar-chart-o fa-fw"></i> University<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <?= $this->Html->link('Add New University', ['controller' => 'Universities', 'action' => 'add']);?>
                        </li>
                        <li>
                            <?= $this->Html->link('View Universities', ['controller' => 'Universities', 'action' => 'index']);?>
                        </li>
                     
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                <li>
                    <a href="javascript:void(0)"><i class="fa fa-bar-chart-o fa-fw"></i> Questions<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <?= $this->Html->link('Add Question Paper', ['controller' => 'QuestionPaperUploads', 'action' => 'add']);?>
                        </li>
                        <li>
                            <?= $this->Html->link('View Question Papers', ['controller' => 'QuestionPaperUploads', 'action' => 'index']);?>
                        </li>
                     
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                </li>
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>
<div id="page-wrapper">
<?= $this->Flash->render() ?>