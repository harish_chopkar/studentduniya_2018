<!-- Modal -->
<div id="addTutionCourseModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <form action="<?= $this->request->webroot; ?>tutions/add-courses" method="POST" id="addTutionCourseFormId">
            <input type="hidden" name="tution_id" value="<?= $tutionId ?>">
                <div class="modal-header">
                <h4 class="modal-title">
                    Add Course
                    <button class="btn btn-sm btn-primary add-more-subject-btn pull-right" type="button"><i class="fa fa-plus"></i> Add More Subjects</button>
                </h4>
                </div>
                <div class="modal-body clearfix course-subject-add-body">
                    <div class="row">
                        <div class="col-md-11">
                            <label for="">Course Name</label>
                            <input type="text" required class="form-control" name="course_name" id="courseNameId">
                        </div>
                        <div class="col-md-1">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-11">
                            <label for="">Course Fees</label>
                            <input type="number" required class="form-control" name="fees" id="feesId">
                        </div>
                        <div class="col-md-1">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-11">
                            <label for="">Start Date</label>
                            <input type="text" required class="form-control" name="start_date" id="startDateId">
                        </div>
                        <div class="col-md-1">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-11">
                            <br/>
                            <label for="">Subject Name</label>
                            <input type="text" class="form-control subject-name" required name="subject_name[]" >
                        </div>
                        <div class="col-md-1">
                            <br/>
                            <br/>
                            <a href="javascript:;" class="remove-add-subjects font-25"><span class="fa fa-times text-danger"></span></a>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" >Proceed</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>

    </div>
</div>