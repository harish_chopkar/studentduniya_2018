<!-- Modal -->
<div id="editTutionCourseModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <form action="<?= $this->request->webroot; ?>tutions/edit-courses" method="POST" id="editTutionCourseFormId">
                <div class="modal-header">
                <h4 class="modal-title">
                    Edit Course
                    <button class="btn btn-sm btn-primary add-edit-more-subject-btn pull-right" type="button"><i class="fa fa-plus"></i> Add More Subjects</button>
                </h4>
                </div>
                <div class="modal-body clearfix course-subject-edit-body">

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" >Update</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>

    </div>
</div>