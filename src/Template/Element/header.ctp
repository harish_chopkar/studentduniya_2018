<nav class="navbar-fixed-top nav-custom " id="header">
    <div class="container no-padding">   
        <div class="col-xs-12">
            <div class="col-md-3 col-xs-5 logo-section clearfix">
            <?php 
                echo $this->Html->image("logo2.svg", 
                    array(
                            "alt" => "Student-Duniya",
                            "class" => "logo",
                            'url' => array(
                                           'controller' => 'Users',
                                           'action' => 'index'
                            )
                    )
                );
            ?>
            </div>
            <div class="navbar-header clearfix">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="navigation col-md-4 col-md-offset-4 col-sm-8 col-xs-4 no-padding">
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right head-nav clearfix col-xs-12 no-padding text-white">
                        <li><?= $this->Html->link('HOME', ['controller' => 'Users', 'action' => 'index', '_full' => true]);?></li>
                        <li><?= $this->Html->link('ABOUT', ['controller' => 'Users', 'action' => 'about', '_full' => true]);?></li>
                        <li><?= $this->Html->link('CONTACT US', ['controller' => 'Users', 'action' => 'contact', '_full' => true]);?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>