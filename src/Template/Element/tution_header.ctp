<div id="wrapper">
    <nav class="navbar navbar-inverse navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?= $this->Html->link('STUDENT_DUNIYA Tution Panel', ['controller' => 'Tutions', 'action' => 'index'], ['class' => 'navbar-brand']);?>
        </div>
        <!-- /.navbar-header -->
        <ul class="nav navbar-top-links navbar-right">
            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li>
                        <?= $this->Html->link(__('Edit Profile'), ['controller'=>'tutions','action' => 'edituser']) ?>
                    </li>
                     <li class="divider"></li>
                    <li>
                        <?php
                            echo $this->Html->link(
                                $this->Html->tag('i', '', array('class' => 'fa fa-sign-out fa-fw')) . "Log Out",
                                array('controller' => 'Users', 'action' => 'logout'),
                                array('class' => 'dropdown-item', 'escape' => false)
                            );
                        ?>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->