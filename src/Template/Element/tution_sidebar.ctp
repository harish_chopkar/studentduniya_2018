    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li>
                    <?php
                        echo $this->Html->link(
                            $this->Html->tag('i', '', array('class' => 'fa fa-dashboard fa-fw')) . " Dashboard",
                            array('controller' => 'Tutions', 'action' => 'index'),
                            array('escape' => false)
                        );
                    ?>
                </li>
                <li>
                    <a href="javascript:void(0)"><i class="fa fa-bar-chart-o fa-fw"></i> Tutions<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <?= $this->Html->link('Add Tution/Branch', ['controller' => 'Tutions', 'action' => 'add']);?>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                <li>
                    <a href="javascript:void(0)"><i class="fa fa-bar-chart-o fa-fw"></i> Cover Photo<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <?= $this->Html->link('Upload Cover Photo', ['controller' => 'Tutions', 'action' => 'coveradd']);?>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>
<div id="page-wrapper" class="clearfix">
<?= $this->Flash->render() ?>