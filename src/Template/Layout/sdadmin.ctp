<!DOCTYPE html>
<html lang="en">
    <head>
        <?= $this->Html->charset() ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">        
        <title>ADMIN_STUDENT-DUNIYA</title>
        <?= $this->Html->meta('favicon.ico','/favicon_sd.ico',['type' => 'icon']); ?>
        <?= $this->Html->css('bootstrap.min.css') ?>
        <?= $this->Html->css('metisMenu.min.css') ?>
        <?= $this->Html->css('sb-admin-2.css') ?>
        <?= $this->Html->css('morris.css') ?>
        <?= $this->Html->css('font-awesome.min.css') ?>
        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
    </head>
    <body>
            <?= $this->element('Adminheader') ?>
            <?= $this->element('Sidebar') ?>
            <?= $this->fetch('content') ?>
            <?= $this->Html->script(['jquery.min','bootstrap.min','metisMenu.min.js','raphael.min']) ?>
        <?= $this->Html->script(['sb-admin-2']) ?>
        <?= $this->fetch('script') ?>
    </body>
</html>