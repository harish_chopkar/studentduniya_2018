<!DOCTYPE html>
<html lang="en">
    <head>
        <?= $this->Html->charset() ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">        
        <title>STUDENT-DUNIYA</title>
        <?= $this->Html->meta('favicon.ico','/favicon_sd.ico',['type' => 'icon']); ?>

        <?= $this->Html->css('bootstrap.min.css') ?>
        <?= $this->Html->css('jquery.bxslider.css') ?>
        <?= $this->Html->css('obase.css') ?>
        <?= $this->Html->css('style.css') ?>
        
        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
    </head>
    <body>
        <!-- <div class="loader"></div> -->
        <?= $this->element('header') ?>   
        <?= $this->fetch('content') ?>
        <?= $this->element('footer') ?>            
        <?= $this->Html->script(['jquery','bootstrap.min','jquery.bxslider.min_optimized','typed','sdcommon']) ?>
        <?= $this->fetch('script') ?>

        <?php if($this->request->action == 'tutionsearch'){ ?> 
            <script type="text/javascript">
                //new update slider
                $(document).ready(function(){
                    if(window.innerWidth <= 640){
                        var count = 1;
                    }else{
                        var count = 3;
                    }
                    $('.slider6').bxSlider({
                        slideWidth:510,
                        minSlides: count,
                        maxSlides: count,
                        moveSlides: 1,
                        slideMargin:10,
                        auto:true,
                        autoControls: true,  
                        pager : false,
                        nextText: '<img src="'+ configpath["globalPath"] + 'img/right-arrow.png" height="40" width="80"/>',
                        prevText: '<img src="'+ configpath["globalPath"] + 'img/left-arrow.png" height="40" width="80"/>'
                    });
                    $('.bx-viewport').css('min-height','140px');
                });               
            </script>
         <?php } ?>
        <?php if($this->request->action == 'index'){ ?> 
            <script type="text/javascript">
                $(document).ready(function(){
                    if(window.innerWidth <= 640){
                        var count = 1;
                    }else{
                        var count = 3;
                    }
                    $('.slider5').bxSlider({
                        slideWidth:510,
                        minSlides: count,
                        maxSlides: count,
                        moveSlides: 1,
                        slideMargin:10,
                        auto:true,
                        autoControls: true,  
                        pager : false,
                        nextText: '<img src="'+ configpath["globalPath"] + 'img/right-arrow.png" height="40" width="80"/>',
                        prevText: '<img src="'+ configpath["globalPath"] + 'img/left-arrow.png" height="40" width="80"/>'
                    });
                    $('.bx-viewport').css('min-height','140px');
                    $('html, body').animate({scrollTop: 0}, 1000);
                    $(".down-arrow").click(function() {
                        $('html,body').animate({
                        scrollTop: $("#info-form").offset().top},
                        'slow');
                    });
                });
                //typing effect on header image
                $(function(){
                    $(".typing-text").typed({
                        strings: ["THE NEEDS OF STUDENTS", "THEIR DIFFICULTIES"],
                        typeSpeed: 100,
                        backDelay: 1200,
                        loop: true,
                    });
                });
            </script>
        <?php } ?>
        <?php if($this->request->action == 'about'){ ?>
            <script>
            //typing effect on header image
              $(function(){
                  $(".typing-text").typed({
                    strings: ["We Understand the needs of Students"],
                    typeSpeed: 100,
                    backDelay: 1200,
                    loop: true,
                  });
              });
            </script>
        <?php } ?> 
        <?php if($this->request->action == 'contact'){ ?>
            <script>
            //typing effect on header image
              $(function(){
                  $(".typing-text").typed({
                    strings: ["YOU HAVE QUESTIONS, WE HAVE ANSWERS", "SOLUTIONS TO KEEP YOU...... ONE STEP AHEAD", "WE ARE ALWAYS HERE TO HEAR FROM YOU", "LOOKING FOR SOME MORE INFORMATION ?"],
                    typeSpeed: 100,
                     backDelay: 1200,
                    loop: true,
                  });
              });
            </script>
        <?php } ?>
        <?php if($this->request->action == 'paper'){ ?>
            <script>
            //typing effect on header image
              $(function(){
                  $(".typing-text").typed({
                    strings: ["We provide all paper according to your need"],
                    typeSpeed: 100,
                     backDelay: 1200,
                    loop: true,
                  });
              });
            </script>
        <?php } ?>
        <?php if($this->request->action == 'tutionclass'){ ?>
        <script>
        //typing effect on header image
            $(function(){
                $(".typing-text").typed({
                    strings: ["Extra efforts for more success."],
                    typeSpeed: 100,
                    backDelay: 1200,
                    loop: true,
                });
            });
        </script>
        <?php } ?> 
    </body>
</html>