<!DOCTYPE html>
<html lang="en">
    <head>
        <?= $this->Html->charset() ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">        
        <title>Login</title>
        <?= $this->Html->meta('favicon.ico','/favicon_sd.ico',['type' => 'icon']); ?>
        <?= $this->Html->css('bootstrap.min.css') ?>
        <?= $this->Html->css('font-awesome.min.css') ?>
        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
    </head>
    <body>
        <!-- <div class="loader"></div> -->
        <?= $this->fetch('content') ?>
        <?= $this->Html->script(['jquery.min','bootstrap.min']) ?>
        <?= $this->Html->script(['sb-admin-2']) ?>
        <?= $this->fetch('script') ?>
    </body>
</html>