<!DOCTYPE html>
<html lang="en">
    <head>
        <?= $this->Html->charset() ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
        <title>Tutions Module</title>
        <?= $this->Html->meta('favicon.ico','/favicon_sd.ico',['type' => 'icon']); ?>

        <?= $this->Html->css('bootstrap.min.css') ?>
        <?= $this->Html->css('metisMenu.min.css') ?>
        <?php if($this->request->action == 'university'){ ?>
            <?= $this->Html->css('dataTables.bootstrap.css') ?>
            <?= $this->Html->css('dataTables.responsive.css') ?>
        <?php } ?>
        <?= $this->Html->css('sb-admin-2.css') ?>
        <?= $this->Html->css('morris.css') ?>
        <?= $this->Html->css('font-awesome.min.css') ?>
        <?= $this->Html->css('style.css') ?>
         <?= $this->Html->script(['jquery.min']) ?>
        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
    </head>
    <body>
            <?= $this->element('tution_header') ?>
            <?= $this->element('tution_sidebar') ?>
            <?= $this->fetch('content') ?>

            <?= $this->Html->script(['jquery.min','bootstrap.min','metisMenu.min.js','raphael.min']) ?>
            <script>
                $(document).ready(function(){
                    $('#dataTables-example').DataTable({
                        responsive: true
                    });
                    alluniversityObj = JSON.parse(alluniversity);
                });
            </script>
        <?= $this->Html->script(['sb-admin-2']) ?>
        <?= $this->fetch('script') ?>
    </body>
</html>