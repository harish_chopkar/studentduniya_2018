<?php
/**
  * @var \App\View\AppView $this
  */
// pr($courses->course_name);

?>

    <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Upload Question Paper of Nagpur University
                        </h1>
                    </div>
    </div>
    <div class="row">
         <p><?= $this->Html->link(__('List Question Paper Uploads'), ['action' => 'index']) ?></p>
    </div>

<div class="col-md-5">
    <?= $this->Form->create($questionPaperUpload, ['type' => 'file']) ?>
    <fieldset>
        <legend><?= __('Upload Question Paper') ?></legend>
        <?php
            echo $this->Form->control('university_id',['type'=>'select','class'=>'form-control reg-field']);?>
            <div class="form-group">
                <label>Course Name</label>
                <select class="form-control reg-field" required="required" name="course_id">
                    <?php foreach($courses as $key => $value):?>
                    <option value="<?php echo $key ?>"><?php echo $value  ?></option>
                    <?php endforeach;?>
                </select>
            </div>
    <div class="form-group">
    <label>Year</label>
    <?php
    $year_options = [
    ['text' => '2010', 'value' => '2010'],
    ['text' => '2011', 'value' => '2011'],
    ['text' => '2012', 'value' => '2012'],
    ['text' => '2013', 'value' => '2013'],
    ['text' => '2014', 'value' => '2014'],
    ['text' => '2015', 'value' => '2015'],
    ['text' => '2016', 'value' => '2016'],
    ['text' => '2017', 'value' => '2017'],
];

        echo $this->Form->select('paper_year',$year_options,['label'=>false, 'class'=>'form-control reg-field']);?>

    </div>
    <div class="form-group">
    <label>Season</label>
    <?php

    $options = [
    ['text' => 'Summer', 'value' => '0'],
    ['text' => 'Winter', 'value' => '1']
];

echo $this->Form->select('season',$options,['label'=>false, 'class'=>'form-control reg-field']);?>
    </div>
    <div class="form-group">
    <label>Subject Name</label>
    <?php echo $this->Form->control('subject_name',['label'=>false, 'placeholder'=>'Subject Name', 'class'=>'form-control reg-field']);?>
    </div>
    <div class="form-group">
    <label>Upload Question Paper</label>
    <?php echo $this->Form->control('file_path',['type'=>'file','label'=>false, 'placeholder'=>'University Description', 'class'=>'form-control reg-field']);?>
    </div>
    </fieldset>
    <?= $this->Form->button(__('Submit'),['class'=>'btn btn-success btn-block']) ?>
    <?= $this->Form->end() ?>
</div>
</div>