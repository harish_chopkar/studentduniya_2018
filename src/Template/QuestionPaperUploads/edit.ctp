<?php
/**
  * @var \App\View\AppView $this
  */
?>
    <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Edit Question Paper of Nagpur University
                        </h1>
                    </div>
    </div>
<div class="questionPaperUploads form large-9 medium-8 columns content">
    <?= $this->Form->create($questionPaperUpload) ?>
    <?php
        // debug($questionPaperUpload);
    ?>
    <fieldset>
        <?php
            echo $this->Form->control('university_id',['type'=>'select','class'=>'form-control reg-field']);?>

            <div class="form-group">
                <label>Course Name</label>
                <select class="form-control reg-field" required="required" name="course_id">
                    <?php foreach($courses as $key => $value):?>
                    <option value="<?php echo $key  ?>" <?= ($questionPaperUpload->course_id == $key ) ? "selected" : "" ?> ><?php echo $value  ?></option>
                    <?php endforeach;?>
                </select>
            </div>
             <div class="form-group">
            <label>Year</label>
                <?php    echo $this->Form->control('paper_year',['label'=>false, 'placeholder'=>'Paper Year', 'class'=>'form-control reg-field']);
                ?>
            </div>
            <div class="form-group">
    <label>Season</label>
    <?php

    $options = [
    ['text' => 'Summer', 'value' => '0'],
    ['text' => 'Winter', 'value' => '1']
    ];

echo $this->Form->select('season',$options,['label'=>false, 'class'=>'form-control reg-field']);?>
    </div>

         <div class="form-group">
            <label>Subject Name</label>
           <?php    echo $this->Form->control('subject_name',['label'=>false, 'placeholder'=>'Subject Name', 'class'=>'form-control reg-field']);?>
        </div>
    </fieldset>
    <?= $this->Form->button(__('Update'),['class'=>'btn btn-success btn-block']) ?>
    <?= $this->Form->end() ?>
</div>
