<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\QuestionPaperUpload[]|\Cake\Collection\CollectionInterface $questionPaperUploads
  */
?>

    <div class="row">
                  <div class="col-lg-12">
                        <h1 class="page-header">
                            Question Paper Details
                        </h1>
                    </div>
    </div>
<div class="col-md-12">
        <p><?= $this->Html->link(__('Add New Question Paper'), ['action' => 'add']) ?></p>
</div>
<div class="questionPaperUploads index large-9 medium-8 columns content">
    <h3><?= __('Question Paper Uploads') ?></h3>
    <table cellpadding="0" cellspacing="0" class="table table-hover table-bordered">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('university_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('course_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('paper_year') ?></th>
                <th scope="col"><?= $this->Paginator->sort('season') ?></th>
                <th scope="col"><?= $this->Paginator->sort('subject_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('File') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($questionPaperUploads as $questionPaperUpload): ?>
            <tr>
                <td><?= $this->Number->format($questionPaperUpload->id) ?></td>
                <td><?= h($questionPaperUpload->university->name) ?></td>
                <td><?= !empty($questionPaperUpload->nagpur_university_course) ? $questionPaperUpload->nagpur_university_course->course_name : $questionPaperUpload->course_id ?></td>
                <td><?= h($questionPaperUpload->paper_year) ?></td>
                <td><?= h( $questionPaperUpload->season ? "Winter" : "Summer" ) ?></td>
                <td><?= h($questionPaperUpload->subject_name) ?></td>
                <td>
                  <?= $this->Form->create(null, ['url' => ['action' => 'download']]);?>
                  <?= $this->Form->hidden('id',['value'=>$questionPaperUpload->file_path]);?>
                  <?= $this->Form->submit('Download',['class'=>'btn btn-info btn-sm']);?>
                  <?= $this->Form->end();?>
                </td>

                <!-- <td><?= h($questionPaperUpload->file_path) ?></td> -->

                <td><?= h($questionPaperUpload->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $questionPaperUpload->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $questionPaperUpload->id], ['confirm' => __('Are you sure you want to delete # {0}?', $questionPaperUpload->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
</div>