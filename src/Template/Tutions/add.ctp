<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="col-md-12">
    <h2 class="text-center">Add <?= empty($mainBranch) ? "Tution" : "Branch"  ?> Details</h2>
    <hr/>
    <?= $this->Form->create($tution,['class'=>'form-horizontal']) ?>
        <?php if (!empty($mainBranch)) : ?>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="col-sm-3">Main Branch/Tution Name</label>
                    <div class="col-sm-8">
                        <?php echo $this->Form->control('parent', ['label' => false, 'placeholder' => 'Tution Name', 'class' => 'form-control', 'disabled' => true, 'value' => $mainBranch->tution_name ]); ?>
                    </div>
                </div>
                <hr>
            </div>
        <?php endif; ?>
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-sm-3"><?= empty($mainBranch) ? "Tution" : "Branch" ?> Name</label>
                <div class="col-sm-8">
                    <?php echo $this->Form->control('tution_name',['label'=>false,'placeholder'=>'Tution Name','class'=>'form-control']);?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3">Tution Description</label>
                <div class="col-sm-8">
                    <?php echo $this->Form->control('tution_description',['type'=>'textarea','label'=>false,'placeholder'=>'Tution Description','class'=>'form-control']);?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3">Owner Name</label>
                <div class="col-sm-8">
                    <?php echo $this->Form->control('owner_name', ['label'=>false,'placeholder'=>'Owner Name','class'=>'form-control']);?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3">Email</label>
                <div class="col-sm-8">
                    <?php echo $this->Form->control('email', ['label'=>false,'placeholder'=>'Email','class'=>'form-control']);?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3">Started IN</label>
                <div class="col-sm-8">
                    <?php echo $this->Form->control('started_in', ['label'=>false,'placeholder'=>'Started In','class'=>'form-control']);?>
                </div>
            </div>
</div>
<div class="col-md-6 form-horizontal">
                        <div class="form-group">
                <label class="col-sm-3">Contact </label>
                    <div class="col-sm-8">
                <?php echo $this->Form->control('contact', ['label'=>false,'placeholder'=>'Contact','class'=>'form-control']);?>
                 </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3">Tag Line</label>
                <div class="col-sm-8">
                    <?php echo $this->Form->control('tag_line', ['label'=>false,'placeholder'=>'Tag Line','class'=>'form-control']);?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3">Street Address</label>
                <div class="col-sm-8">
                    <?php echo $this->Form->control('street_address', ['label'=>false,'placeholder'=>'Street Address','class'=>'form-control']);?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3">District</label>
                <div class="col-sm-8">
                    <select name="district" class="form-control">
                        <?php foreach ($districts as $key => $district):?>
                        <option value="<?php echo $district->district_name; ?>"><?php echo $district->district_name; ?></option>
                        <?php endforeach;?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3">Landmark</label>
                <div class="col-sm-8">
                    <?php echo $this->Form->control('landmark', ['label'=>false,'placeholder'=>'Landmark','class'=>'form-control']);?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3">PIN</label>
                <div class="col-sm-8">
                    <?php echo $this->Form->control('zip_code', ['label'=>false,'placeholder'=>'PIN','class'=>'form-control']);?>
                </div>
            </div>
             <div class="form-group">
                 <div class="col-sm-8 col-md-offset-3">
                    <?= $this->Form->button(__('Submit'),['class'=>'btn btn-success btn-block']) ?>
                </div>
             </div>

</div>
<?= $this->Form->end() ?>
</div>
