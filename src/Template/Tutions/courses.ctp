<div class="col-md-12">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-9">
                <h3 class="text-center"><?= $tution->tution_name; ?></h3>
            </div>
            <div class="col-md-3">
                <h3><?= $this->Html->link(__('Add New Course'), '#', ['class' => 'btn btn-primary btn-md pull-right', 'data-toggle' => "modal", 'data-target' => "#addTutionCourseModal"]) ?>
                </h3>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <hr>
            <table class="table table-responsive table-border table-striped table-hover">
                <thead>
                    <tr>
                        <th>Course Name</th>
                        <th>Fees (Rs)</th>
                        <th>Start Date</th>
                        <th>Subject Name</th>
                        <th style="width:150px"></th>
                    </tr>
                </thead>
                <?php foreach ($tution->tution_courses as  $course): ?>
                    <tr>
                        <th class="text-primary"><?php echo $course->course_name ?></th>
                        <td><?php echo $course->fees ?></td>
                        <td><?php echo !empty($course->start_date) ? date('d-m-Y', strtotime($course->start_date)) : '---' ?></td>
                        <td class="">
                            <?php foreach ($course->course_subjects as $subject): ?>
                                <div><?php echo $subject->subject_name; ?></div>
                            <?php endforeach; ?>
                        </td>
                        <td>
                            <a href="javascript:;" title="Delete Course" class="font-25 delete-course-link text-danger" data-course-id="<?php echo $course->id ?>" data-url="<?= $this->request->webroot ?>tutions/delete-tution-course"><span class="fa fa-times pull-right"></span></a>
                            <a href="javascript:;" title="Edit Course" class="font-25 edit-course-link" data-course-id="<?php echo $course->id ?>" data-url="<?= $this->request->webroot ?>tutions/edit-tution-course-details"><span class="fa fa-pencil-square-o pull-right"></span></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
</div>

<?= $this->element('Tution/add_course_modal'); ?>
<?= $this->element('Tution/edit_course_modal'); ?>

<?= $this->Html->script(['custom/tution-course']); ?>