<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="col-md-12">
    <h2 class="text-center"><?= __('Upload Cover Photo') ?></h2>
    <hr/>
   <div class="col-md-12">
    <?= $this->Form->create($tution,['type'=>'file', 'class'=>'form-horizontal']) ?>      <div class="form-group text-center">
             <img id="banneroutput" class="topper-dashimg" style="height: 200px; width: 100%" />
          </div>
            <div clas s="form-group">
                <label class="col-sm-3">Upload Cover Photo</label>
                <div class="col-sm-5">
                    <?php echo $this->Form->control('cover_photo', ['type'=>'file', 'label'=>false,'placeholder'=>'Cover Photo','id'=>'bannerImg','class'=>'form-control','onchange'=>'loadBanner(event)']);?>
                </div>
            </div>
            <div class="form-group">
                 <div class="col-sm-5 col-md-offset-3">
                    <?= $this->Form->button(__('Submit'),['class'=>'btn btn-success btn-block']) ?>
                </div>
             </div>
</div>
<?= $this->Form->end() ?>
</div>
<script>
  var loadBanner = function(event) {
    var output = document.getElementById('banneroutput');
    output.src = URL.createObjectURL(event.target.files[0]);
  };
  $(document).ready(function(){
              $('#bannerImg').click(function(e){
                  $("input[type='file']").trigger('click');
                  $("input[type='file']").change(function(e){
                      var img = $('<img/>', {
                        id: 'imgShow',                        
                    });  
                      var fileName = e.target.files[0].name;
                      if(fileName != ''){
                          $('#bannerImg').html(fileName);
                          var file = this.files[0];
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('#imgShow').attr('src', e.target.result);
                            console.log(img.attr('src', e.target.result));
                        }        
                        reader.readAsDataURL(file);
                     }
                  });
              });
          });
</script>