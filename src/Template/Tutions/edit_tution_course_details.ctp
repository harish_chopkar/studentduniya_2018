<input type="hidden" id="#editCourseId" name="course_id" value="<?= $course->id ?>">
<input type="hidden" name="tution_id" value="<?= $course->tution_id ?>">
<div class="row">
    <div class="col-md-11">
        <label for="">Course Name</label>
        <input type="text" class="form-control" required name="course_name" value="<?= $course->course_name ?>" id="editCourseNameId">
    </div>
    <div class="col-md-1">
    </div>
</div>

<div class="row">
    <div class="col-md-11">
        <label for="">Course Fees</label>
        <input type="number" required class="form-control" name="fees" value="<?= $course->fees ?>" id="feesId">
    </div>
    <div class="col-md-1">
    </div>
</div>
<div class="row">
    <div class="col-md-11">
        <label for="">Start Date</label>
        <input type="text" required class="form-control" value="<?= !empty($course->start_date) ? date('d-m-Y', strtotime($course->start_date)) : '' ?>" name="start_date" id="startDateId">
    </div>
    <div class="col-md-1">
    </div>
</div>

<?php foreach($course->course_subjects as $subject): ?>
    <div class="row row-edit-subject">
        <div class="col-md-11">
            <br/>
            <label for="">Subject Name</label>
            <input type="hidden" name="subject_id[]" value="<?= $subject->id ?>">
            <input type="text" required class="form-control edit-subject-name" value="<?= $subject->subject_name ?>" name="subject_name[]" >
        </div>
        <div class="col-md-1">
            <br/>
            <br/>
            <a href="javascript:;" class="remove-edit-subjects font-25"><span class="fa fa-times text-danger"></span></a>
        </div>
    </div>
<?php endforeach; ?>
