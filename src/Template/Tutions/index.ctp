<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Tution[]|\Cake\Collection\CollectionInterface $tutions
  */
?>
<div class="col-md-12">
    <?php if(empty($tutions->toArray())){?>
        <h2><?= $this->Html->link(__('Add Tutions Details'), ['action' => 'add'],['class'=>'btn btn-success btn-lg']) ?></h2>
        <h3>Welcome to Student Duniya.</h3>
        <p>Take your First Step by adding Tution Details.</p>

    <?php }else{ ?>
    <?php foreach ($tutions as $tution): ?>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-9">
                        <h3 class="text-center"><?= h($tution->tution_name) ?></h3>
                    </div>
                    <div class="col-md-3">
                        <h3><?= $this->Html->link(__('Edit Details'), ['action' => 'edit', $tution->id], ['class' => 'btn btn-primary btn-sm']) ?>
                        &nbsp;
                        <?= $this->Html->link(__('Course Details'), ['action' => 'courses', $tution->id], ['class' => 'btn btn-info btn-sm']) ?>
                        </h3>
                    </div>
                </div>
            </div>
            <?php if (empty($tution->parent_id) && !empty($tution->cover_photo)): ?>
                <div class="row">
                    <div class="col-md-12">
                        <img src="<?= $tution->cover_photo ?>" height="200" width="100%">
                    </div>
                </div>
            <?php endif; ?>
            <table class="table table-hover table-bordered">
                <tbody>

                    <tr>
                        <th>Tution Name</th>
                        <td><?= h($tution->tution_name) ?></td>
                        <th>Owner Name</th>
                        <td><?= h($tution->owner_name) ?></td>
                    </tr>
                    <tr>
                        <th>Tag Line</th>
                        <td colspan="3"><?= h($tution->tag_line) ?></td>
                    </tr>
                    <tr>
                        <th>Tution Description</th>
                        <td colspan="3"><?= h($tution->tution_description) ?></td>
                    </tr>
                    <tr>
                        <th>Contact</th>
                        <td><?= h($tution->contact) ?></td>
                        <th>Email</th>
                        <td><?= h($tution->email) ?></td>
                    </tr>
                    <tr>
                        <th>Tution Started In</th>
                        <td colspan="3"><?= h($tution->started_in) ?></td>
                    </tr>
                    <tr>
                        <th>Street Address</th>
                        <td><?= h($tution->street_address) ?></td>
                        <th>Landmark</th>
                        <td><?= h($tution->landmark) ?></td>
                    </tr>
                    <tr>
                        <th>District</th>
                        <td><?= h($tution->district) ?></td>
                        <th>PIN</th>
                        <td><?= $this->Number->format($tution->zip_code) ?></td>
                    </tr>
            </tbody>
        </table>
    </div>
        <?php endforeach; ?>
        <div class="clearfix"></div>
<?php } ?>
</div>
</div>