<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Tution[]|\Cake\Collection\CollectionInterface $tutions
  */
?>
<div class="col-md-12">
 
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-12">
                    <h2 class="text-center"><?= h($tution->tution_name) ?></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php echo $this->Html->image('../'.$tution->cover_photo, ['alt' => 'CakePHP', 'height'=>'200px', 'width'=>'100%']); ?>
            </div>
        </div>
        <table class="table table-hover table-bordered">
            <tbody>
                  
                   <tr>
                    <th>Tution Name</th>
                    <td><?= h($tution->tution_name) ?></td>
                    <th>Owner Name</th>
                    <td><?= h($tution->owner_name) ?></td>
                </tr> 
                <tr>
                    <th>Tag Line</th>
                    <td colspan="3"><?= h($tution->tag_line) ?></td>
                </tr> 
                <tr>
                    <th>Tution Description</th>
                    <td colspan="3"><?= h($tution->tution_description) ?></td>
                </tr>
                <tr>
                    <th>Contact</th>
                    <td><?= h($tution->contact) ?></td>
                    <th>Email</th>
                    <td><?= h($tution->email) ?></td>
                </tr> 
                <tr>
                    <th>Tution Started In</th>
                    <td colspan="3"><?= h($tution->started_in) ?></td>
                </tr>
                <tr>
                    <th>Street Address</th>
                    <td><?= h($tution->street_address) ?></td>
                    <th>Landmark</th>
                    <td><?= h($tution->landmark) ?></td>
                </tr>    
                <tr>
                    <th>District</th>
                    <td><?= h($tution->district) ?></td>
                     <th>PIN</th>
                    <td><?= $this->Number->format($tution->zip_code) ?></td>
                </tr>        
        </tbody>
    </table>
      
</div>
</div>  