<?php
/**
  * @var \App\View\AppView $this
  */
?>
    <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Edit University
                        </h1>
                    </div>
                </div>
<div class="col-md-5">
    <?= $this->Form->create($university) ?>
    <fieldset>
        <legend><?= __('Add University') ?></legend>
        <div class="form-group">
           <label>University Name</label>
        <?php echo $this->Form->control('name',['label'=>false, 'placeholder'=>'University Name *', 'class'=>'form-control reg-field']);?>
    </div>
    <div class="form-group">
           <label>University Description</label>
        <?php echo $this->Form->control('description',['type'=>'textarea','label'=>false, 'placeholder'=>'University Description', 'class'=>'form-control reg-field']);?>
    </div>
    </fieldset>
    <?= $this->Form->button(__('Submit'),['class'=>'btn btn-success']) ?>
    <?= $this->Form->end() ?>
</div>
</div>