<?php
/**
  * @var \App\View\AppView $this
  */
?>
    <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Edit University
                        </h1>
                    </div>
                </div>
<div class="col-md-5">
    <?= $this->Form->create($university) ?>
    <fieldset>
        <legend><?= __('Edit University') ?></legend>
        <div class="form-group">
           <label>University Name</label>
        <?php echo $this->Form->control('name',['label'=>false, 'placeholder'=>'First Name *', 'class'=>'form-control reg-field']);?>
    </div>
    <div class="form-group">
           <label>Description</label>
        <?php echo $this->Form->control('description',['label'=>false, 'placeholder'=>'First Name *', 'class'=>'form-control reg-field']);?>
    </div>
    </fieldset>
    <?= $this->Form->button(__('Submit'),['class'=>'btn btn-success']) ?>
     <?= $this->Html->link(__('Back'), ['action' => 'index'],['class'=>'btn btn-warning']) ?>
    <?= $this->Form->end() ?>
</div>
</div>