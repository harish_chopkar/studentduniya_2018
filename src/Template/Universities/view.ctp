<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\University $university
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit University'), ['action' => 'edit', $university->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete University'), ['action' => 'delete', $university->id], ['confirm' => __('Are you sure you want to delete # {0}?', $university->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Universities'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New University'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Question Paper Uploads'), ['controller' => 'QuestionPaperUploads', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Question Paper Upload'), ['controller' => 'QuestionPaperUploads', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="universities view large-9 medium-8 columns content">
    <h3><?= h($university->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($university->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Description') ?></th>
            <td><?= h($university->description) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($university->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created Date') ?></th>
            <td><?= h($university->created) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Question Paper Uploads') ?></h4>
        <?php if (!empty($university->question_paper_uploads)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('University Id') ?></th>
                <th scope="col"><?= __('Course Id') ?></th>
                <th scope="col"><?= __('Paper Year') ?></th>
                <th scope="col"><?= __('Season') ?></th>
                <th scope="col"><?= __('Subject Name') ?></th>
                <th scope="col"><?= __('File Path') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($university->question_paper_uploads as $questionPaperUploads): ?>
            <tr>
                <td><?= h($questionPaperUploads->id) ?></td>
                <td><?= h($questionPaperUploads->university_id) ?></td>
                <td><?= h($questionPaperUploads->course_id) ?></td>
                <td><?= h($questionPaperUploads->paper_year) ?></td>
                <td><?= h($questionPaperUploads->season) ?></td>
                <td><?= h($questionPaperUploads->subject_name) ?></td>
                <td><?= h($questionPaperUploads->file_path) ?></td>
                <td><?= h($questionPaperUploads->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'QuestionPaperUploads', 'action' => 'view', $questionPaperUploads->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'QuestionPaperUploads', 'action' => 'edit', $questionPaperUploads->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'QuestionPaperUploads', 'action' => 'delete', $questionPaperUploads->id], ['confirm' => __('Are you sure you want to delete # {0}?', $questionPaperUploads->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
</div>