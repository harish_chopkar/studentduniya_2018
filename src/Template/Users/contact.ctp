<!-- Start header -->
<header class="contact">
	<div class="header-banner container-fluid">
		<div class="container text-white">
			<h1 class="margin20">CONTACT US</h1>
			<h3 class="margin-side10 text-white"><span class="typing-text"></span></h3>
		</div>
	</div>
</header>
<section>
	<div class="contact-section container-fluid padding30">
		<div class="enquiry-details">
   			<div class="container">
      			<div class="col-sm-7 col-xs-12 padding30 padding15">
          			<h2>ENQUIRY FORM</h2>
          			<p class="lg">Ask any of your quries regarding your Education and career. We are here to help you out. Feel free to write us.</p>
          			<div class="col-xs-12 no-padding contact-form margin20">
            			<?= $this->Form->create($contactUs) ?>
              				<div class="form-group col-xs-6  reg-form padding-left0">
               					<!-- <input type="text"  placeholder="Your First Name *" required> -->
                        <?php echo $this->Form->control('fname',['label'=>false, 'placeholder'=>'First Name *', 'class'=>'form-control reg-field']);?>
              				</div>
              				<div class="form-group col-xs-6  reg-form">
              				<?php echo $this->Form->control('lname',['label'=>false, 'placeholder'=>'Last Name *', 'class'=>'form-control reg-field']);?>
                      </div>
              				<div class="form-group col-xs-6  reg-form padding-left0">
                		  <?php echo $this->Form->control('education',['label'=>false, 'placeholder'=>'Education *', 'class'=>'form-control reg-field']);?>
              				</div>
              				<div class="form-group col-xs-6  reg-form">
                        <?php echo $this->Form->control('college_name',['label'=>false, 'placeholder'=>'School/College Name *', 'class'=>'form-control reg-field']);?>
                          
                        </div>
              				<div class="form-group col-xs-6  reg-form padding-left0">
                				<?php echo $this->Form->control('email',['label'=>false, 'placeholder'=>'Your Email *', 'class'=>'form-control reg-field']);?>

              				</div>
              				<div class="form-group col-xs-6  reg-form">
                        <?php echo $this->Form->control('mobile',['label'=>false, 'placeholder'=>'Your Phone No. *', 'class'=>'form-control reg-field']);?>
                				
              				</div>
              				<div class="form-group col-xs-12 reg-form padding-left0">
              				<?php echo $this->Form->control('message',['type'=>'textarea', 'label'=>false, 'placeholder'=>'Your Message *', 'class'=>'form-control message']);?>
                      </div>
              				<div class="form-group col-xs-12 text-left">
              					<button type="submit" class="btn btn-info view-btn">Submit</button>
              				</div>
            			<?= $this->Form->end() ?>
          			</div>
        		</div>
        		<div class="col-sm-5 col-xs-12 padding30 padding15">
                   <div class="col-xs-12">
                      <h2>ADDRESS</h2>
                      <div class="address">
                        <p class="xl no-margin"><span class="icon-placeholder"></span>Lahanuji Nagar</p>
                        <p class="lg">Civil line behind head post office, Wardha. 442001</p>
                        <p class="lg"><span class="icon-old-typical-phone"></span> +91 - 8856034131</p>
                         <p class="lg indent20"> +91 - 7972260199</p>
                         <p class="lg indent20"> +91 - 7276366128</p>
                        <p class="lg"><span class="icon-close-envelope"></span> info@studentduniya.com</p>
                      </div>
                    </div>
                </div>
      		</div>
    	</div>
  	</div>
</section>
