<div class="container-fluid" >
    <?= $this->Flash->render() ?>
    <div class="col-sm-4 col-sm-offset-4 col-xs-12 bg-primary">
        <div class="text-center">
            <?php 
                echo $this->Html->image("logo3.svg", 
                    array(
                            "alt" => "Student-Duniya",
                            "class" => "logo",
                            'url' => array(
                                           'controller' => 'Users',
                                           'action' => 'index'
                            )
                    )
                );
            ?>
        </div>
        <div class="no-padding">
			<h1 class="text-center">Login</h1>
			<?= $this->Form->create() ?>
        <div class="form-group col-xs-12  reg-form padding-left0">
            <?php echo $this->Form->control('email',['label'=>false, 'placeholder'=>'Email *', 'class'=>'form-control reg-field']);?>
        </div>
        <div class="form-group col-xs-12  reg-form padding-left0">
            <?php echo $this->Form->control('password',['label'=>false, 'placeholder'=>'Password *', 'class'=>'form-control reg-field']);?>
		</div>
        <div class="form-group col-xs-12 text-left">
            <?= $this->Form->button('Login',['class'=>'btn btn-success']) ?>
        </div>
			<?= $this->Form->end() ?>
        </div>
    </div>
</div>