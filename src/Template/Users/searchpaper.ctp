<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\University $university
  */
?>
    <!-- Start header -->
    <header class="paper">
        <div class="header-banner container-fluid">
            <div class="container text-white">
                <h1 class="margin20">PAPER/ENGINEERING</h1>
                <h3 class="margin-side10 text-white"><span class="typing-text"></span></h3>
            </div>
        </div>
    </header>
        <!-- information form -->
      <div class="container-fluid info-formsection quick-find" id="info-form">
        <div class="col-sm-12 no-padding text-center text-white no-padding">
          <h2 class="margin20">QUICK FIND</h2>
          <div class="container col-xs-12 no-margin text-center margin20 no-padding">
            <?= $this->Form->create(null, ['url' => ['action' => 'searchpaper'],'class'=>'form-inline col-xs-12 no-padding']);?>
            <div class="form-group margin-side10 no-padding">
                <select class="form-control" required="required" name="paper_year">
                  <option>2010</option>
                  <option>2011</option>
                  <option>2012</option>
                  <option>2013</option>
                  <option>2014</option>
                  <option>2015</option>
                  <option>2016</option>
                  <option>2017</option>
                </select>
                    <select class="form-control" required="required" name="course_id">
                    <?php foreach($courses as $key => $value):?>
                    <option value="<?php echo $key ?>"><?php echo $value?></option>
                    <?php endforeach;?>
                </select>
            <?= $this->Form->submit('Search',['class'=>'btn btn-default btn-search']);?>
            <?= $this->Form->end();?>
            </div>
         </div>
        </div>
      </div>
<div class="row">
<div class="col-md-12">
    <h3 class="text-center">Rashtrasant Tukadoji Maharaj Nagpur University</h3>
    <div class="related">
        <h4 class="text-center"><?= __('Question Papers') ?></h4>
        <?php if (!empty($questions_paper)): ?>
            <?php foreach ($questions_paper as $question_paper): ?>
            <div class="col-sm-4 col-xs-12">
                  <div class="paper-categ paper-subject-div">
                    <div class="paper-type text-center">
                      <h2><?= h($question_paper->subject_name) ?></h2>
                      <h4><?= !empty($question_paper->nagpur_university_course->course_name) ? $question_paper->nagpur_university_course->course_name : h($question_paper->course_id); ?></h4>
                      <h5><?= h($question_paper->season?"Winter":"Summer") ?></h5>
                      <h5><span class="icon-days type-icon"></span><?= h($question_paper->paper_year) ?></h5>
                    </div>
                    <div class="paper-download-div">
                  <?= $this->Form->create(null, ['url' => ['action' => 'download']]);?>
                  <?= $this->Form->hidden('id',['value'=>$question_paper->file_path]);?>
                  <?= $this->Form->submit('Download',['class'=>'btn btn-info btn-sm']);?>
                  <?= $this->Form->end();?>
                    </div>
                  </div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>
</div>