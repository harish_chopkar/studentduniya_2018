<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\University $university
  */
?>
    <!-- Start header -->
    <header class="paper">
        <div class="header-banner container-fluid" <?= (!empty($tutions->toArray()[0]->cover_photo)) ? "style='background-image:url(../../{$tutions->toArray()[0]->cover_photo})'" : "" ?>>
            <div class="container text-white">
                <h1 class="margin20"><?= h($tutions->toArray()[0]->tution_name) ?></h1>
                <h3 class="margin-side10 text-white"><span class="typing-text"></span></h3>
            </div>
        </div>
    </header>
        <!-- information form -->
      <div class="container-fluid info-formsection quick-find" id="info-form">

      </div>
<div class="row">
<div class="col-md-12">
    <?php foreach ($tutions as $tution): ?>
        <div class="row">
            <div class="col-md-12">
                    <h2 class="text-center"><?= h($tution->tution_name) ?></h2>
            </div>
        </div>
        <div class="row">
            <?php
                if (!empty($tution->cover_photo)) {
            ?>
                <!-- <div class="col-md-12">
                    <img src="../../<?= $tution->cover_photo ?>" height="250" width="100%" alt="Tution Image">
                </div> -->
            <?php    }
            ?>
            <div class="col-md-12">
                <h4 class="text-center"><?= h($tution->tag_line) ?></h4>
                <hr/>
            <!--<div class="col-md-4 col-md-offset-2">
                    <p> <b>Tution Started In :</b> <?= h($tution->started_in) ?> </p>
                    <p> <b>Owner :</b> <?= h($tution->owner_name) ?> </p>
                    <p> <b>Tution Description :</b> <?= h($tution->tution_description) ?> </p>
            </div>
            <div class="col-md-4">

                    <p><b>Contact :</b> <?= h($tution->contact) ?></p>
                    <p><b>Email :</b> <?= h($tution->email) ?></p>


                    <p><b>Street Address :</b> <?= h($tution->street_address) ?></p>
                    <p><b>Landmark :</b> <?= h($tution->landmark) ?></p>


                    <p><b>District :</b> <?= h($tution->district) ?></p>
                    <p><b>PIN :</b> <?= $this->Number->format($tution->zip_code) ?></p>

            </div> -->
            <table class="table table-hover table-bordered">
                <tbody>

                    <tr>
                        <th class="text-primary">Tution Name</th>
                        <td><?= h($tution->tution_name) ?></td>
                        <th class="text-primary">Owner Name</th>
                        <td><?= h($tution->owner_name) ?></td>
                    </tr>
                    <tr>
                        <th class="text-primary">Tag Line</th>
                        <td colspan="3"><?= h($tution->tag_line) ?></td>
                    </tr>
                    <tr>
                        <th class="text-primary">Tution Description</th>
                        <td colspan="3"><?= h($tution->tution_description) ?></td>
                    </tr>
                    <tr>
                        <th class="text-primary">Contact</th>
                        <td><?= h($tution->contact) ?></td>
                        <th class="text-primary">Email</th>
                        <td><?= h($tution->email) ?></td>
                    </tr>
                    <tr>
                        <th class="text-primary">Tution Started In</th>
                        <td colspan="3"><?= h($tution->started_in) ?></td>
                    </tr>
                    <tr>
                        <th class="text-primary">Street Address</th>
                        <td><?= h($tution->street_address) ?></td>
                        <th class="text-primary">Landmark</th>
                        <td><?= h($tution->landmark) ?></td>
                    </tr>
                    <tr>
                        <th class="text-primary">District</th>
                        <td><?= h($tution->district) ?></td>
                        <th class="text-primary">PIN</th>
                        <td><?= $this->Number->format($tution->zip_code) ?></td>
                    </tr>
            </tbody>
        </table>

        <hr>
        <h4>Courses & Subjects Information</h4>

        <table class="table table-responsive table-border table-striped table-hover">
                <thead>
                    <tr>
                        <th class="text-primary">Course Name</th><th class="text-primary">Subject Name</th>
                    </tr>
                </thead>
                <?php foreach ($tution->tution_courses as $course) : ?>
                    <tr>
                        <th class="text-primary"><?php echo $course->course_name ?></th>
                        <td class="">
                            <?php foreach ($course->course_subjects as $subject) : ?>
                                <div><?php echo $subject->subject_name; ?></div>
                            <?php endforeach; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
            <hr>
        </div>
        </div>
        <?php endforeach; ?>
</div>
</div>