<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\University $university
  */
?>
    <!-- Start header -->
    <header class="paper">
        <div class="header-banner container-fluid">
            <div class="container text-white">
                <h1 class="margin20">Tution Classes Details</h1>
                <h3 class="margin-side10 text-white"><span class="typing-text"></span></h3>
            </div>
        </div>
    </header>
        <!-- information form -->
      <div class="container-fluid info-formsection quick-find" id="info-form">

      </div>
<div class="row">
    <div class="col-md-12">
        <hr>
        <form action="">
            <div class="form-group col-md-12">
                <input type="text" class="form-control" name="subject" required placeholder="Search Subject" value="<?= $keyword ?>">
            </div>
        </form>
    </div>
    <div class="col-md-12">
       <?php foreach ($tutionData as $key => $value) {?>
         <div class="col-md-4 col-xs-12 tution-page-div">
           <div class="tution-class tution-class-div">
             <div class="tution-class-ppr-type">
               <p><?php echo "<b>Tution Name</b>: <span>$value->tution_name</span>"?></p>
               <p><?php echo "<b>Owner Name</b>: <span>$value->owner_name</span>"?></p>
               <p><?php echo "<b>Street Address</b>: <span>$value->street_address</span>"?></p>
               <p><?php echo "<b>District</b>: <span>$value->district</span>"?></p>
               <p><?php echo "<b>PIN code</b>: <span>$value->zip_code</span>"?></p>
               <p><?php echo "<b>Contact</b>: <span>$value->contact</span>"?></p>
               <p><?= $this->Html->link(__('View Details'), ['action' => 'viewTuitionDetails', $value->id],['class'=>'btn btn-info view-btn']) ?></p>
             </div>
           </div>
        </div>
       <?php } ?>
       <?php
            if (empty($tutionData->toArray())) {
                echo "<h4>No Record Found</h4>";
            }
       ?>

 </div>
</div>












