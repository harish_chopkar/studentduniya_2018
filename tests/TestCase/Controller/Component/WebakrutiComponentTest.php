<?php
namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\WebakrutiComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Component\WebakrutiComponent Test Case
 */
class WebakrutiComponentTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Controller\Component\WebakrutiComponent
     */
    public $Webakruti;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->Webakruti = new WebakrutiComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Webakruti);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
