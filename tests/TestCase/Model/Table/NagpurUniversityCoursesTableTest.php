<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NagpurUniversityCoursesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NagpurUniversityCoursesTable Test Case
 */
class NagpurUniversityCoursesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\NagpurUniversityCoursesTable
     */
    public $NagpurUniversityCourses;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.nagpur_university_courses'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('NagpurUniversityCourses') ? [] : ['className' => NagpurUniversityCoursesTable::class];
        $this->NagpurUniversityCourses = TableRegistry::get('NagpurUniversityCourses', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->NagpurUniversityCourses);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
