<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NotifyTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NotifyTable Test Case
 */
class NotifyTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\NotifyTable
     */
    public $Notify;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.notify'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Notify') ? [] : ['className' => NotifyTable::class];
        $this->Notify = TableRegistry::get('Notify', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Notify);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
