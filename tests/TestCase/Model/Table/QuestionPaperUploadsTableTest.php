<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\QuestionPaperUploadsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\QuestionPaperUploadsTable Test Case
 */
class QuestionPaperUploadsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\QuestionPaperUploadsTable
     */
    public $QuestionPaperUploads;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.question_paper_uploads',
        'app.universities',
        'app.courses'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('QuestionPaperUploads') ? [] : ['className' => QuestionPaperUploadsTable::class];
        $this->QuestionPaperUploads = TableRegistry::get('QuestionPaperUploads', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->QuestionPaperUploads);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
