<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TutionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TutionsTable Test Case
 */
class TutionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TutionsTable
     */
    public $Tutions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tutions',
        'app.users',
        'app.user_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Tutions') ? [] : ['className' => TutionsTable::class];
        $this->Tutions = TableRegistry::get('Tutions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Tutions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
