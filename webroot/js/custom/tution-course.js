$(document).ready(function(){

    $(document).on('click', '.add-more-subject-btn', function(e){
        var subjectHtml =   '<div class="row"><div class="col-md-11">'+
                                '<br />'+
                                '<label for="">Subject Name</label>'+
                                '<input type="text" required class="form-control subject-name" name="subject_name[]" >'+
                            '</div>'+
                            '<div class="col-md-1">'+
                                '<br />'+
                                '<br />'+
                                '<a href="javascript:;" class="remove-add-subjects font-25"><span class="fa fa-times text-danger"></span></a>'+
                            '</div></div>';

        $(".course-subject-add-body").append(subjectHtml);
    });

    $(document).on('click', '.add-edit-more-subject-btn', function(e){
        var subjectHtml =   '<div class="row row-edit-subject">'+
                                '<div class="col-md-11">'+
                                    '<br/>'+
                                    '<label for="">Subject Name</label>'+
                                    '<input type="text" required class="form-control edit-subject-name" name="subject_name[]">'+
                                '</div>'+
                                '<div class="col-md-1">'+
                                    '<br/>'+
                                    '<br/>'+
                                    '<a href="javascript:;" class="remove-edit-subjects font-25"><span class="fa fa-times text-danger"></span></a>'+
                                '</div>'+
                            '</div>';
        $(".course-subject-edit-body").append(subjectHtml);

    });

    $(document).on('submit', '#addTutionCourseFormId', function(e){

        if ($("input.subject-name").length < 1) {
            alert("Please add subject");
            e.preventDefault();
            return false;
        }

    });

    $(document).on('submit', "#editTutionCourseFormId", function(e){

        if ($("input.edit-subject-name").length < 1) {
            alert("Please add subject");
            e.preventDefault();
            return false;
        }
    });

    $(document).on("click", ".remove-add-subjects", function(e){
        $(this).parent().parent().replaceWith("");
    });

    $(document).on("click", ".remove-edit-subjects", function(e){
        $(this).parent().parent().replaceWith("");
    });

    $(document).on('click', '.edit-course-link', function(e){
        var courseId = $(this).attr("data-course-id");

        $(".course-subject-edit-body").html("");
        $.ajax({
            url: $(this).attr('data-url') + "/" + courseId,
            type: 'GET',
            method: 'POST',
            dataType: 'html',
            success: function (response) {
                $("#editTutionCourseModal").modal('show');
                $(".course-subject-edit-body").html(response);
            },
            error: function (err) {

            }
        });
    });

    $(document).on('click', '.delete-course-link', function(e){
        if (!confirm("Do you really want to delete this course ?")) {
            return false;
        }
        var courseId = $(this).attr("data-course-id");
        $.ajax({
            url : $(this).attr('data-url') + "/" + courseId,
            type: 'GET',
            method: 'POST',
            dataType: 'json',
            success: function(response){
                console.log(response);
                if (typeof response.response.success != "undefined" && response.response.success) {
                    window.location.reload();
                } else {
                    alert("Unable to delete the course !");
                }
            },
            error: function(err) {

            }
        });
    });
});